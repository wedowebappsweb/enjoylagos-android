import React, { Component } from 'react'
import { Text, Picker, View, Image, Dimensions, StyleSheet,NetInfo, AsyncStorage, Platform, ToastAndroid } from 'react-native';
import { Container, Content, Card, CardItem, Left, Thumbnail, Body, Icon, Form, Item, Label, Input, Button, Spinner, ActionSheet } from 'native-base';

import DatePicker from 'react-native-datepicker';
import PhoneInput from 'react-native-phone-input';
import Toast from 'react-native-toast-native';

class Register extends Component {

  constructor(props){
    super(props)
    this.state = {
        isLoading: false,
        showToast: false,
        name: '',
        email: '',
        password: '',
        confpassword: '',
        phone: '',
        passwordInputType: true
    }
    this._onChangeEmail = this._onChangeEmail.bind(this);
    
  }

    componentDidMount(){
        NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
        this.setState({
            deviceToken: global.deviceToken
        });
    }

    _handleConnectionChange = (isConnected) => {
        
        if(isConnected == false) {
            ToastAndroid.show("Please check your internet connection.",Toast.SHORT); 
        }
    };

    componentWillMount(){
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
    }


    render () {
        return (
        <Content>
            <View>
                <Form>
                    <Item floatingLabel>
                        <Icon active name='ios-person' style={styles.icon} /> 
                        <Label style={{ paddingTop:6}}>Name</Label>
                        <Input onChangeText={(text) => this.setState({ name: text })} />
                    </Item>
                    <Item floatingLabel style={{marginTop : 10}}>
                        <Icon active name='ios-mail' style={styles.icon} /> 
                        <Label style={{ paddingTop:6}}>Email</Label>
                        <Input autoCapitalize='none' value={this.state.email} onChangeText={(text) => this._onChangeEmail( text )} />
                    </Item>
                
                    <Item style={{ borderBottomColor:'gray'}}>
                        <View style={{flexDirection:"row", paddingTop: 18, marginTop:18, paddingBottom:10 }}>
                            <View style={{flex:1, flexDirection: "row"}}>
                                <Icon active name='ios-call' style={{color: '#0f7482',paddingTop:0}} /> 
                                <Label>Phone</Label>
                            </View>
                            <View style={{flex:3, paddingTop:6}}>
                                <PhoneInput
                                    ref={(ref) => { this.phone = ref; }}
                                    onPressFlag={this.onPressFlag}
                                    onChangePhoneNumber= {(number) => this.setState({phone: number})}
                                    offset={20}
                                    initialCountry='ng'
                                />
                            </View>
                        </View>
                    </Item>
                    <View style={{flexDirection:'row'}}>
                        <Item floatingLabel style={{flex:1, borderBottomColor:'gray',borderBottomWidth:.5}}>
                        <Icon name='ios-lock' style={styles.icon} />
                        <Label style={{ paddingTop:6}}>Password</Label>
                        <Input secureTextEntry={this.state.passwordInputType} onChangeText={(text) => this.setState({ password: text })} />
                        </Item>
                        <View style={{paddingTop:40, flex:.2, borderBottomColor:'gray',borderBottomWidth:.5}}>
                            <Icon name={this.state.passwordInputType ? 'ios-eye' : 'ios-eye-off'} style={{ color:'#0f7482'}} onPress={this.showhidePassword} />
                        </View>
                    </View>
                    <Item floatingLabel>
                        <Icon active name='ios-lock' style={styles.icon} /> 
                        <Label style={{ paddingTop:6}}>Confirm Password</Label> 
                        <Input secureTextEntry={this.state.passwordInputType} onChangeText={(text) => this.setState({ confpassword: text })} />
                    </Item>
                    <View style={{ padding: 16 }}>
                        {
                            this.state.isLoading == true ? 
                            <Spinner color="#0f7482" /> : 
                            <Button block success onPress={this.registeruser} style={{ backgroundColor : '#0f7482' }}>
                            <Text style={{ color: "#ffffff", fontSize:18 }}>Sign Up</Text>
                            </Button>
                        }                
                    </View>
                </Form>
                <Text style={styles.promotionText}>Signing up gives you access to exclusive deals on the app and in your mail.</Text>
            </View>
        </Content>
        )
    }


    showhidePassword = () => {
        if(this.state.passwordInputType) {
          this.setState({passwordInputType: false});
        } else {
          this.setState({passwordInputType: true});
        }
    }
    
    _onChangeEmail = (text) =>  {
        this.setState({ email: text });
    } 

    registeruser = () => {
        this.setState({
            isLoading: true
        });

        const data = {
            name : this.state.name,
            email : this.state.email,
            password : this.state.password,
            phone : this.state.phone,
            country_code: '+234'
        }

        let errorMsg = '';
        let errorFlag = false;
        
        if(data.password != this.state.confpassword){
            errorFlag = true;
            errorMsg = 'Password does not match.';
        }

        if(data.password.length < 6){
            errorFlag = true;
            errorMsg = 'Password must be at least 6 characters long';
        }
        if(data.password == ''){
            errorFlag = true;
            errorMsg = 'Please enter password';
        }
        if(data.phone == ''){
            errorFlag = true;
            errorMsg = 'Please enter phone';
        }
        if(data.country_code == ''){
            errorFlag = true;
            errorMsg = 'Select country code';
        }

        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(reg.test(data.email) === false) {
            errorFlag = true;
            errorMsg = 'Please enter valid email address';
        }
        if(data.email == ''){
            errorFlag = true;
            errorMsg = 'Please enter email address';
        }
        if(data.name == '') { 
            errorFlag = true;
            errorMsg = 'Please enter name';
        }
        if(errorFlag) {
            
            this.setState({
                isLoading: false
            });

            ToastAndroid.show(errorMsg,Toast.SHORT); 
            
            return true;
        }

        const json = JSON.stringify(data);
        var DeviceInfo = require('react-native-device-info');

        fetch( global.BASEURL + '/wp-json/auth/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'os': Platform.OS,
                'appv': DeviceInfo.getVersion(),
                'osv': DeviceInfo.getSystemVersion(),
                'lang':DeviceInfo.getDeviceLocale(),
                'devicetoken' : this.state.deviceToken,
                'devicename': DeviceInfo.getBrand()+' '+DeviceInfo.getModel(),
            },
            body: json
        })
        .then((response) => response.json())
        .then((response) => {
        if(response.status == 200){
            // try {
            AsyncStorage.setItem('user_token', response.data.token);
            AsyncStorage.setItem('user_name', response.data.name);
            AsyncStorage.setItem('user_phone', response.data. phone);
            global.access_token = response.data.token; 
            global.user_name = response.data.name;
            // AsyncStorage.setItem('@MySuperStore:user_data',  JSON.stringify(response.data));
            AsyncStorage.setItem('userData',  JSON.stringify(response.data));
            
            this.setState({
                isLoading: false
            });
      
            ToastAndroid.show("You are successfully registered and verification email has been sent to your register E-mail ID.",Toast.SHORT); 
            
            this.props.navigation.navigate('Home');
        } else {
            this.setState({
                isLoading: false
            });

            ToastAndroid.show(response.message,Toast.SHORT); 
            
        }
        })
    }

}

const styles = {
  promotionText: {
    fontSize: 16,
    color: '#575757',
    padding: 20,
    textAlign:'center'
  },
  icon:{
    color: '#0f7482',
    paddingTop:10,
  }
}

export default Register;