import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Content, Text, Tab, Tabs, TabHeading } from 'native-base';

import AppHeader from '../../components/Header';
import Login from './Login';
import Register from './Register';

export default class Account extends Component {
  
  render() {
    
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Enjoy Lagos" showBack={true}  showAccount={true} noShadow={true} />
        <Tabs tabBarPosition = 'top' tabBarUnderlineStyle={{backgroundColor:'#ffffff'}} style={styles.tab}>
          <Tab heading={ <TabHeading style={styles.TabHeading} ><Text style={styles.TabHeadingText}>SIGN UP</Text></TabHeading>}>
            <Register navigation={this.props.navigation} />
          </Tab>
          <Tab heading={ <TabHeading style={styles.TabHeading}><Text style={styles.TabHeadingText}>LOGIN</Text></TabHeading>}>
            <Login navigation={this.props.navigation} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const styles = {
  TabHeadingText:{
      color: '#ffffff',
  },
  TabHeadingFilterText:{
      color: '#ffffff',
  },
  TabHeading:{
      backgroundColor: '#0f7482',
  },
  TabHeadingFilter:{
      backgroundColor: '#f5ad2a'
  }
}