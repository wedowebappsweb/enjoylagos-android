import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Container, Content, Text, Tab, Tabs, TabHeading } from 'native-base';

import AppHeader from '../../components/Header';

import Profile from '../../components/Account/Profile';
import Wallet from '../../components/Account/Wallet';
import OrderHistory from '../../components/Account/OrderHistory';

const userIcon = require('../../components/img/profile_img/user.png');
const walletIcon = require('../../components/img/profile_img/wallet.png');
const orderIcon = require('../../components/img/profile_img/order_history.png');

export default class ManageAccount extends Component {

    render() {

        return (
        <Container>
            <AppHeader navigation={this.props.navigation} noShadow={true} title="Manage Account" showBack={false} showAccount={true} />
            <Tabs tabBarPosition = 'top' initialPage={ global.wallet ? 1 : 0} tabBarUnderlineStyle={{backgroundColor:'#ffffff'}} style={styles.tab}>
                <Tab heading={ <TabHeading style={styles.TabHeading} ><Text style={styles.TabHeadingText}> <Image style={styles.menuItemIcon} source={userIcon} /> Profile</Text></TabHeading>}>
                    <Profile navigation={this.props.navigation} />
                </Tab>
                <Tab heading={ <TabHeading style={styles.TabHeading}><Text style={styles.TabHeadingText}> <Image style={styles.menuItemIcon} source={walletIcon} /> Wallet</Text></TabHeading>}>
                    <Wallet  navigation={this.props.navigation} />
                </Tab>
                <Tab heading={ <TabHeading style={styles.TabHeading}><Text style={styles.TabHeadingText}> <Image style={styles.menuItemIcon} source={orderIcon} />  History</Text></TabHeading>}>
                    <OrderHistory  navigation={this.props.navigation} />
                </Tab>
            </Tabs>
        </Container>
        );
    }
}

const styles = {
    TabHeadingText:{
        color: '#ffffff',
        fontSize:18
    },
    TabHeadingFilterText:{
        color: '#ffffff',
    },
    TabHeading:{
        backgroundColor: '#0f7482',
    },
    TabHeadingFilter:{
        backgroundColor: '#f5ad2a'
    },
    menuItemIcon:{
        width:40,
        height:40,
        // marginTop:4
    },
}