import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet,NetInfo, TouchableHighlight, Modal, AsyncStorage, Platform, ToastAndroid } from 'react-native'
import { Container, Content, Card, CardItem, Left, Thumbnail, Body, Icon, Form, Item, Label, H2, Input, Button, Spinner} from 'native-base';

import DatePicker from 'react-native-datepicker';
import Toast from 'react-native-toast-native';

class Login extends Component {

  constructor(props){
    super(props)
    this.state = {
        isLoading: false,
        showToast: false,
        email: '',
        password: '',
        modalVisible: false,
        forgotPasswordEmail:'',
        passwordInputType: true
    }
    this._onChangeEmail = this._onChangeEmail.bind(this);
  }

  setModalVisible(visible) {
      this.setState({modalVisible: visible});
  }

  componentDidMount(){
    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
    this.setState({
      deviceToken: global.deviceToken
    });
  }

  _handleConnectionChange = (isConnected) => {
    
    if(isConnected == false) {
      
      ToastAndroid.show("Please check your internet connection.",Toast.SHORT); 
      
    }
  };

  componentWillMount(){
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
  }

  forgotPassword = () => {
    this.setState({isLoading:true});
    const data = {
      email: this.state.forgotPasswordEmail
    }
    const json = JSON.stringify(data);
    
    if(this.state.forgotPasswordEmail == ''){
      
      ToastAndroid.show('Please enter valid email address.'); 
      
      this.setState({isLoading:false});
      return true;
    }
    fetch( global.BASEURL + '/wp-json/api/forgotUserPass', {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
      },
      body: json
  })
  .then(response => response.json())
  .then((response) => {
      
      if(response.status == 200){
        
          this.setState({
              isLoading: false
          });
          ToastAndroid.show(response.message,Toast.SHORT ); 
      
          this.setModalVisible(false);
        
      } else {
          this.setState({
              isLoading: false
          });
          
          ToastAndroid.show(response.message,Toast.SHORT); 
          
      }
    })
  .catch(error => {
  
  })
  }

  render () {
    return (
      <View>
        <Form>
          <Item floatingLabel>
            <Icon active name='ios-mail' style={styles.icon} /> 
            <Label style={{ paddingTop:6}}>Email</Label>
            <Input value={this.state.email} autoCapitalize = 'none' onChangeText={(text) => this._onChangeEmail(text)} />
          </Item>
          <View style={{flexDirection:'row'}}>
              <Item floatingLabel style={{flex:1,borderBottomWidth:.5, borderBottomColor:'gray'}}>
              <Icon name='ios-lock' style={styles.icon} />
              <Label style={{ paddingTop:6}}>Password</Label>
              <Input secureTextEntry={this.state.passwordInputType} onChangeText={(text) => this.setState({ password: text })} />
              </Item>
              <View style={{paddingTop:40, flex:.2, borderBottomColor:'gray', borderBottomWidth:.5}}>
                  <Icon name={this.state.passwordInputType ? 'ios-eye' : 'ios-eye-off'} style={{ color:'#0f7482'}} onPress={this.showhidePassword} />
              </View>
          </View>
          <View style={{ padding: 16 }}>
              {
                this.state.isLoading == true ? 
                <Spinner color="#0f7482" /> : 
                <Button block success onPress={this.loginuser} style={{ backgroundColor : '#0f7482' }}>
                  <Text style={{ color: "#ffffff", fontSize:18 }}>Login</Text>
                </Button>
              
              }                
          </View>
        </Form>
        <View>
            <Text style={{ color:'#0f7482', padding:20, fontSize:14, textDecorationLine: "underline" }} onPress={() => this.setModalVisible(true)}> Forgot Password </Text>
        </View>
        <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                  this.setModalVisible(false)
              }}>
              <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                  <View style={{ padding: 30, backgroundColor: '#fff', height: undefined, width: undefined }}>
                      <Form>
                          <H2>Enter your email address here.</H2>
                          <Item floatingLabel>
                              <Icon active name='ios-mail' style={styles.icon} /> 
                              <Label>Email</Label>
                              <Input autoCapitalize='none' onChangeText={(text) => this.setState({ forgotPasswordEmail: text})} />
                          </Item>

                          <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                          
                              <View style={{margin:10}}>
                                  {
                                      this.state.isLoading == true ?
                                      <Spinner color="#0f7482" /> : 
                                      <Button block onPress={this.forgotPassword} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                          <Text style={{ color: "#ffffff", fontSize:18 }}> Send Email </Text>
                                      </Button>
                                  }
                              </View>

                              <View style={{margin:10}}>
                                  <Button block onPress={() => this.setModalVisible(false)} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                      <Text style={{ color: "#ffffff", fontSize:18 }}> Cancel </Text>
                                  </Button>
                              </View>
                          </View>
                      </Form>
                  </View>
              </View>
          </Modal>
      </View>
    )
  }

  _onChangeEmail = (text) =>  {
    this.setState({ email: text });
  } 

  showhidePassword = () => {
    if(this.state.passwordInputType) {
      this.setState({passwordInputType: false});
    } else {
      this.setState({passwordInputType: true});
    }
  }
  loginuser = () => {
    this.setState({
      isLoading: true
    });

    const data = {
      username : this.state.email,
      password : this.state.password,
    }
    let errorMsg = '';
    let errorFlag = false;
    
    if(data.password == ''){
      errorFlag = true;
      errorMsg = 'Please enter password';
    }
    
    // if(data.password.length < 8){
    //   errorFlag = true;
    //   errorMsg = 'Password must be at least 8 characters long';
    // }

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    if(reg.test(data.username) === false) {
      errorFlag = true;
      errorMsg = 'Please enter valid email address';
    }
    if(data.username == ''){
      errorFlag = true;
      errorMsg = 'Please enter email address';
    }
    if(errorFlag == true){
      this.setState({
          isLoading: false
      });
      
      ToastAndroid.show(errorMsg,Toast.SHORT); 
      return false;
    }
    const json = JSON.stringify(data);
    var DeviceInfo = require('react-native-device-info');
    fetch( global.BASEURL + '/wp-json/auth/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'os': Platform.OS,
          'appv': DeviceInfo.getVersion(),
          'osv': DeviceInfo.getSystemVersion(),
          'lang':DeviceInfo.getDeviceLocale(),
          'devicetoken' : this.state.deviceToken,
          'devicename': DeviceInfo.getBrand()+' '+DeviceInfo.getModel(),
        },
        body: json 
    })
    .then((response) => response.json())
    .then((response) => {
      console.log(response);
      if(response.status == 200){
        // try {
          AsyncStorage.setItem('user_token', response.data.token);
          AsyncStorage.setItem('user_name', response.data.name);
          AsyncStorage.setItem('user_phone', response.data.phone);
          AsyncStorage.setItem('userData',  JSON.stringify(response.data));
          global.access_token = response.data.token;
          global.user_name = response.data.name;
        //   } catch (error) {
        //   console.log('AsyncStorage error: ' + error.message);
        // }
        this.setState({
          isLoading: false
        });
        
        if(global.vendorScreen || global.bookTour) {
            if(global.vendorScreen) {
              const rowData = this.props.navigation.state.params.rowData.rowData;
              const deal_name = this.props.navigation.state.params.rowData.deal_name;
              this.props.navigation.navigate('Deals',{rowData,deal_name});
              global.vendorScreen = false;
            } else {
              const rowData = this.props.navigation.state.params.rowData;
              this.props.navigation.navigate('Lagostour',{rowData});
              global.bookTour = false;
            }
            
        } else {
          this.props.navigation.navigate('Home');
        }
        
      } else {
        this.setState({
          isLoading: false
        });
        
        ToastAndroid.show(response.message,Toast.SHORT); 
      
      }
    })
  }
}

const styles = {
  icon:{
    color: '#0f7482',
    paddingTop:12,
    paddingRight: 10, 
  }
}

export default Login;