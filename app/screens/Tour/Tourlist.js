import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Content, Text } from 'native-base';

import AppHeader from '../../components/Header';
import Tourlist from '../../components/Tour/Tourlist';

export default class tourlist extends Component {
  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Lagos Tours" showBack={false} showAccount={true} />
        <Tourlist  navigation={this.props.navigation} />
      </Container>
    );
  }
}