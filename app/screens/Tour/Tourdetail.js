import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Content, Text, Tab, Tabs, TabHeading } from 'native-base';

import AppHeader from '../../components/Header';

import Tourdetails from '../../components/Tour/Tourdetails';
import Tourimage from '../../components/Tour/Tourimage';
import Tourbook from '../../components/Tour/Tourbook';

export default class Tourdetail extends Component {

  render() {
    console.log(this.props.navigation.state.params.rowData);
    const { ID, post_title, post_content, tour_price, thumbnail } = this.props.navigation.state.params.rowData;

    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title={post_title} showBack={true} showAccount={true} />
        <Tourimage thumbnail={thumbnail} />
        <Tabs tabBarPosition = 'top' tabBarUnderlineStyle={{backgroundColor:'#ffffff'}} style={styles.tab}>
          <Tab heading={ <TabHeading style={styles.TabHeading} ><Text style={styles.TabHeadingText}>Tour Details</Text></TabHeading>}>
            <Tourdetails tour_price={tour_price} post_content={post_content} post_title={post_title} />
          </Tab>
          <Tab heading={ <TabHeading style={styles.TabHeading}><Text style={styles.TabHeadingText}>Book Tour</Text></TabHeading>}>
            <Tourbook navigation={this.props.navigation} tour_id={ID} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const styles = {
  TabHeadingText:{
      color: '#ffffff',
  },
  TabHeadingFilterText:{
      color: '#ffffff',
  },
  TabHeading:{
      backgroundColor: '#0f7482',
  },
  TabHeadingFilter:{
      backgroundColor: '#f5ad2a'
  }
}