import React, { Component } from 'react';
import { ActivityIndicator, View, ListView, AsyncStorage } from 'react-native';
import { Container, Header, Title, Content, List, ListItem, Thumbnail, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Spinner, Tab, Tabs, TabHeading } from 'native-base';

import AppHeader from '../components/Header';
import HomeSlider from '../components/HomeSlider';
import HomeTabs from '../components/HomeTabs';

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }
  
  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Deals & Offers" showBack={false} showAccount={true} />
        <HomeSlider navigation={this.props.navigation}/>
        <HomeTabs navigation={this.props.navigation} />
      </Container>
    );
  }
}