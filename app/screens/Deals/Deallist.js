import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Container, Content, Text, Icon } from 'native-base';

import ActionButton from 'react-native-action-button';

import AppHeader from '../../components/Header';
import HomeTabList from '../../components/HomeTabList.js';

import {
  Analytics,
  Hits as GAHits,
  Experiment as GAExperiment
} from 'react-native-google-analytics';
import DeviceInfo from 'react-native-device-info';
var ga = this.ga = null;

export default class Deallist extends Component {

  constructor(props) {
    super(props);
    this.state = {
      active : false
    }
  }

  navigateDealDetails(rowData) {
    this.props.navigation.navigate('Dealdetails',{rowData});
  }

  componentDidMount() {
    let clientId = DeviceInfo.getUniqueID();
    
    ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
    
    var screenView = new GAHits.ScreenView(
      'EnjoyLagos App',
      'Deal Screen',
      DeviceInfo.getReadableVersion(),
      DeviceInfo.getBundleId()
    );
    ga.send(screenView);
  }

  render() {
    var params = this.props.navigation.state.params;
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} noShadow={true} noSearch={true} title="Deals & Offers" showBack={false} showAccount={true} />
        <HomeTabList params={{}} sortBy='dist' navigation={this.props.navigation} showSearch={true} controlPanel={true} params={params} />
        <ActionButton buttonColor="#0f7482" onPress={() => this.props.navigation.navigate('Dealcategories')} icon={<Icon name='ios-funnel' style={styles.actionButtonIcon} />}>
        </ActionButton>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});