import React, { Component } from 'react';
import { AppRegistry, AsyncStorage, View, StyleSheet, TouchableOpacity, Modal, ToastAndroid } from 'react-native';
import { Container,H2,H4, Content, Text, Tab, Header, Thumbnail, Form, Item, Label, Input, Tabs, Left, Right, Card, Spinner, CardItem, Body, Image, Icon, Button, TabHeading } from 'native-base';

import AppHeader from '../../components/Header';
import DealsAPI from '../../utils/Deals';
import Communications from 'react-native-communications';
import PhoneInput from 'react-native-phone-input';
import Toast from 'react-native-toast-native';

      
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';

class VendorDetails extends Component {
    
    constructor(props) {
        super(props);
        this.buyDeal = this.buyDeal.bind(this);
        this.state = {
            isLoading: true,
            vendorDetails:null,
            name:'',
            phone:'',
            email:'',
            buyProcess:false,
            access_token:null, 
            isLoggedIn : true, 
            modalVisible: false,
            orderPlaced: false,
            vendorModelVisible:false,
            verifyModalVisible: false,
            verifyLoading: false
        }
      }

    _dealVendor(callback){

        let deal_data = this.props.navigation.state.params.rowData;

        DealsAPI.getDealVendor(callback,deal_data.deal_id, deal_data.id); 
    }
    
    componentDidMount(){  

        this._dealVendor(responseJson => {
            
            if(responseJson.status == 200){
                this.setState({
                    isLoading: false,
                    vendorDetails: responseJson
                });
            } else {
                this.setState({
                    isLoading: false,
                    vendorModelVisible: true
                });
            }
        });

        AsyncStorage.getItem('userData', (err, result) => {
            
            if( result != null ) {
                let userData = JSON.parse(result);
                this.setState({
                    name: userData.name,
                    email: userData.email,
                    phone: userData.phone,
                    access_token: userData.token
                });
            } else {
                this.setState({
                    isLoggedIn: false
                });
            }
                
        })


    
    }

    _sendVerification = () => {
        this.setState({
            verifyLoading: true
        });
        fetch( global.BASEURL + '/wp-json/auth/verifyAccount', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'authorization':this.state.access_token
            },
        })
        .then((response) => response.json())
        .then((response) => {

            setTimeout(function(){
                if(response.status == 200){
                    ToastAndroid.show("Verification link has been sent to your register E-mail ID.",Toast.SHORT); 
                } else {
                    ToastAndroid.show(response.message,Toast.SHORT); 
                }
            },600)
            this.setState({
                verifyModalVisible: false,
                verifyLoading: false
            });
        }).catch(error => {
            
        })
    }

    buyDeal = () => {
        
        if(this.state.access_token == '' || this.state.access_token == null){
            const rowData = this.props.navigation.state.params;
            this.props.navigation.navigate('Account',{rowData});
            global.vendorScreen = true;
            
            ToastAndroid.show('Please login to buy deal.',Toast.SHORT); 
            
        } else {
            this.setState({
                buyProcess: true
            });
            
            let deal_data = this.props.navigation.state.params.rowData;
            
            const data = {
                user_name: this.state.name,    
                email: this.state.email,    
                phone: this.state.phone,    
                deal_id: deal_data.deal_id,
                item_id: deal_data.id,
                price: deal_data.price,
            }
            
            const json = JSON.stringify(data);
    
            let errorMsg = '';
            let errorFlag = false;

            if(data.user_name == ''){
                errorFlag = true;
                errorMsg = 'Please enter name';
            }
    
            if(data.phone == ''){
                errorFlag = true;
                errorMsg = 'Please enter phone';
            }
    
            if(data.email == ''){
                errorFlag = true;
                errorMsg = 'Please enter email';
            }
    
            if(errorFlag) {
                ToastAndroid.show(errorMsg,Toast.SHORT); 
            
                return true;
            } else {
                
                fetch( global.BASEURL + '/wp-json/auth/buyNow', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'authorization':this.state.access_token
                    },
                    body: json 
                })
                .then((response) => response.json())
                .then((response) => {
                    if(response.status == 401) {
                        this.setState({
                            verifyModalVisible : true,
                            buyProcess: false
                        })

                    } else {
                        if(response.status == 200){
                            this.setState({
                                buyProcess: false
                            });
                            
                            let paymeny_notif = {
                                price: data.price,
                                title: "Order History",
                                post_title: "Your purchase has been processed successfully.",
                                description: "₦" + data.price + " has been deducted from your wallet." 
                            }
                            
                            FCM.presentLocalNotification({
                                // id: "UNIQ_ID_STRING",                           // (optional for instant notification)
                                title: "Your purchase has been processed successfully.",                     // as FCM payload
                                body: "₦" + paymeny_notif.price + " " +"- Deducted from your wallet.",                    // as FCM payload (required)
                                sound: "default",                                   // as FCM payload
                                priority: "high",                                   // as FCM payload
                                click_action: "com.myapp.MyCategory",               // as FCM payload - this is used as category identifier on iOS.
                                badge: 0,                                           // as FCM payload IOS only, set 0 to clear badges
                                icon: "ic_launcher",                                // as FCM payload, you can relace this with custom icon you put in mipmap
                                data: paymeny_notif,                                           // extra data you want to throw
                                show_in_foreground: true,
                                // local: true 
                                // show_in_foreground                               // notification when app is in foreground (local & remote)
                            });
                            
                            this.setState({
                                orderPlaced: true
                            })

                        } else {
                            this.setState({
                                buyProcess: false,
                                modalVisible: true
                            });
                        }
                    }
                    
                }).catch(error => {
                    
                })
            }
        }
        
    }

    render() {
        const dealVendor = this.state.vendorDetails;

        const { content, price } = this.props.navigation.state.params.rowData;
        if (this.state.isLoading) {
            return (
                <Container>
                    <AppHeader navigation={this.props.navigation} noShadow={true} title={this.props.navigation.state.params.deal_name} showBack={true} showAccount={true} />
                        <Spinner color="#0f7482" />
                </Container>
            );
        }

        return (
        <Container>
            <AppHeader navigation={this.props.navigation} noShadow={true} title={this.props.navigation.state.params.deal_name} showBack={true} showAccount={true} />
                <Content>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        // alert('Modal has been closed.');
                    }}>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                        <View style={{ padding: 30, backgroundColor: '#fff' }}>
                            <Form>
                                <Text style={{color: 'red', fontSize: 16, textAlign: 'center'}}>Insufficient funds in your wallet.</Text>
                                <Text style={{fontSize: 14, textAlign: 'center'}}>Would like to top-up your wallet now ?</Text>

                                <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                                
                                    <View style={{margin:10}}>
                                        <Button block success onPress={() => {
                                            this.props.navigation.navigate('ManageAccount'); global.wallet = true;
                                        }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                            <Text style={{ color: "#ffffff", fontSize:14 }}> Continue </Text>
                                        </Button>
                                    </View>
                                    <View style={{margin:10}}>
                                        <Button block danger onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }} style={{ padding:10 }}>
                                            <Text style={{ color: "#ffffff",fontSize:14 }}> Cancel </Text>
                                        </Button>
                                    </View>
                                </View>

                            </Form>
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.verifyModalVisible}
                    onRequestClose={() => {
                        this.setState({
                            verifyModalVisible: false
                        })
                    }}>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                        <View style={{ padding: 30, backgroundColor: '#fff' }}>
                            <Form>
                                {/* <Text style={{color: 'red', fontSize: 16, textAlign: 'center'}}>Insufficient funds in your wallet.</Text> */}
                                <Text style={{fontSize: 18, textAlign: 'center'}}>You have to verify your email before purchase can be made.</Text>

                                <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                                
                                    <View style={{margin:10}}>
                                        <Button block success onPress={() => {
                                            this.setState({ verifyModalVisible: false });
                                        }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                            <Text style={{ color: "#ffffff",fontSize:14 }}> OK </Text>
                                        </Button>
                                    </View>

                                    <View style={{margin:10}}>
                                        { this.state.verifyLoading ? <Spinner color="#0f7482" /> :
                                        <Button block success onPress={() => {
                                            this._sendVerification()
                                        }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                            <Text style={{ color: "#ffffff", fontSize:14 }}> Resend Verification </Text>
                                        </Button>
                                        }
                                    </View>
                                </View>

                            </Form>
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.orderPlaced}
                    onRequestClose={() => {
                        // alert('Modal has been closed.');
                    }}>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                        <View style={{ padding: 30, backgroundColor: '#fff' }}>
                            <Form>
                                {/* <Text style={{color: 'red', fontSize: 16, textAlign: 'center'}}>Insufficient funds in your wallet.</Text> */}
                                <Text style={{fontSize: 18, textAlign: 'center'}}>Your order has been placed successfully.</Text>

                                <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                                
                                    <View style={{margin:10}}>
                                        <Button block success onPress={() => {
                                            this.setState({ orderPlaced: false });
                                        }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                            <Text style={{ color: "#ffffff",fontSize:14 }}> Okay </Text>
                                        </Button>
                                    </View>
                                </View>

                            </Form>
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.vendorModelVisible}
                    onRequestClose={() => {
                        // alert('Modal has been closed.');
                        this.setState({ vendorModelVisible:false})
                    }}>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                        <View style={{ padding: 30, backgroundColor: '#fff' }}>
                            <Form>
                                {/* <Text style={{color: 'red', fontSize: 16, textAlign: 'center'}}>Insufficient funds in your wallet.</Text> */}
                                <Text style={{fontSize: 18, textAlign: 'center'}}>This deal is only available to purchase at the merchants location.</Text>

                                <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                                
                                    <View style={{margin:10}}>
                                        <Button block success onPress={() => {
                                            this.setState({ vendorModelVisible: false });
                                        }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                            <Text style={{ color: "#ffffff",fontSize:14 }}> Okay </Text>
                                        </Button>
                                    </View>
                                </View>

                            </Form>
                        </View>
                    </View>
                </Modal>

                <View style={{flex:1, flexDirection: 'column', justifyContent: 'space-between', padding:10}}>
                    <View style={styles.TabHeadingText}>
                        <Left>
                            <Text style={styles.TabHeadingFilterText}>{ content }</Text>
                        </Left>
                        <Right>
                            <Text style={styles.TabHeadingFilterText}> ₦ { price }</Text>
                        </Right>
                    </View>
                    
                </View>
                {this.state.isLoggedIn && this.state.vendorDetails != null ?
                <View>
                <Card style={{flex:10, flexDirection:'column', margin:10}}>
                    <CardItem style={{ borderBottomWidth:1, borderBottomColor:'#0f7482'}}>
                        <Text style={styles.TabHeadingFilterText}> Vendor Details </Text>
                    </CardItem>
                    
                    <CardItem>
                        <Left>
                        <Thumbnail source={{uri: dealVendor.user_thumb }} />
                            <Body>
                                <Text style={styles.TabHeadingFilterText}> { dealVendor.user_name } </Text>
                                {/* <Text note>GeekyAnts</Text> */}
                            </Body>
                        </Left>
                    </CardItem>
                    
                    <CardItem>
                            <Icon style={styles.icon} name="ios-mail" />
                            <TouchableOpacity onPress={() => Communications.email([dealVendor.user_email], null, null, null, null)}>
                                <Text style={styles.TabHeadingFilterText}> { dealVendor.user_email ? dealVendor.user_email.trim() : '' } </Text>
                            </TouchableOpacity>
                    </CardItem>
                    
                    <CardItem>
                            <Icon style={styles.icon} name="ios-call" />
                            <TouchableOpacity onPress={() => Communications.phonecall(dealVendor.user_phone, true)}>
                                <Text style={styles.TabHeadingFilterText}> { dealVendor.user_phone }</Text>
                            </TouchableOpacity>
                    </CardItem>

                    <CardItem>
                        <Icon style={styles.icon} name="ios-home" />
                        <Text style={styles.TabHeadingFilterText}> { dealVendor.user_address ? dealVendor.user_address.trim() : '' }</Text>
                    </CardItem>

                </Card>

                    <Form>
                        <Item floatingLabel>
                            <Icon active name='ios-person' style={styles.icon} /> 
                            <Label>Name</Label>
                            <Input value={ this.state.name } onChangeText={(text) => this.setState({ name: text })} />
                        </Item>
                        <Item floatingLabel>
                            <Icon active name='ios-mail' style={styles.icon} /> 
                            <Label>Email</Label>
                            <Input value={ this.state.email } onChangeText={(text) => this.setState({ email: text })} />
                        </Item>
                    
                        <Item>
                            <View style={{flexDirection:"row", paddingTop: 20, marginTop:20, paddingBottom:10, borderBottomWidth:.3 }}>
                                <View style={{flex:1, flexDirection: "row"}}>
                                    <Icon active name='ios-call' style={styles.icon} /> 
                                    <Label>Phone</Label>
                                </View>
                                <View style={{flex:3}}>
                                    <PhoneInput
                                        ref={(ref) => { this.phone = ref; }}
                                        onPressFlag={this.onPressFlag}
                                        onChangePhoneNumber= {(number) => this.setState({phone: number})}
                                        offset={20}
                                        value={this.state.phone }
                                        initialCountry='ng'
                                    />
                                </View>
                            </View>
                        </Item>
                        
                        <View style={{flex:1,padding:20}}>
                            {this.state.buyProcess ? 
                                <Spinner color="#0f7482" />
                            :
                                <Button block success onPress={this.buyDeal} style={{ backgroundColor : '#0f7482' }}>
                                    <Text style={{ color: "#ffffff" }}>Buy</Text>
                                </Button>
                            } 
                        </View>
                    </Form>
                    </View>
                    :
                    <View style={{flex:1,padding:20}}>
                        { this.state.vendorDetails != null ? 
                        <Button block success onPress={() => { 
                            global.vendorScreen = true;
                            const rowData = this.props.navigation.state.params;
                            this.props.navigation.navigate('Account',{rowData});
                        }} style={{ backgroundColor : '#0f7482' }}>
                            <Text style={{ color: "#ffffff" }}>Login to Buy</Text>
                        </Button> 
                        : <Text></Text>}
                        
                    </View>
                }
                </Content>
        </Container>
        );
    }
}

const styles = {
    TabHeadingText:{
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 5,
    },
    TabHeadingFilterText:{
        color: '#0f7482',
        justifyContent: 'space-between',
        fontSize: 16,
    },
    icon : {
        color: '#0f7482',
    }
}

export default VendorDetails;
AppRegistry.registerComponent('VendorDetails', () => RNCommunications);
