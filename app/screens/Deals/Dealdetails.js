import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Content, Text, Tab, Tabs, TabHeading } from 'native-base';

import AppHeader from '../../components/Header';

import Dealabout from '../../components/Deal/Dealabout';
import Dealimage from '../../components/Deal/Dealimage';
import Dealitems from '../../components/Deal/Dealitems';

export default class Dealdetails extends Component {

    render() {
        const { ID, post_title, description, address, loc_lat, loc_lng, phone, email, location, thumbnail } = this.props.navigation.state.params.rowData;
        return (
            <Container>
                <AppHeader navigation={this.props.navigation} title={post_title} showBack={true} showAccount={true} />
                <Dealimage thumbnail={thumbnail} />
                <Tabs tabBarPosition = 'top' tabBarUnderlineStyle={{backgroundColor:'#ffffff'}} style={styles.tab}>
                    <Tab heading={ <TabHeading style={styles.TabHeading} ><Text style={styles.TabHeadingText}>Available Deals</Text></TabHeading>}>
                        <Dealitems title={post_title} navigation={this.props.navigation} deal_id={ID} />
                    </Tab>
                    <Tab heading={ <TabHeading style={styles.TabHeading}><Text style={styles.TabHeadingText}>About Us</Text></TabHeading>}>
                        <Dealabout description={description} lat={loc_lat} lng={loc_lng} address={address} phone={phone} email={email} />
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

const styles = {
  TabHeadingText:{
      color: '#ffffff',
  },
  TabHeadingFilterText:{
      color: '#ffffff',
  },
  TabHeading:{
      backgroundColor: '#0f7482',
  },
  TabHeadingFilter:{
      backgroundColor: '#f5ad2a'
  }
}