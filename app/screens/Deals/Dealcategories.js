import React, { Component } from 'react';
import { View, StyleSheet, ListView, NetInfo, ToastAndroid } from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Spinner, Left, Right,Button, Icon, Body, Title } from 'native-base';

import AppHeader from '../../components/Header';
import Toast from 'react-native-toast-native';

import DealsAPI from '../../utils/Deals';
import renderIf from '../../functions/renderif';

export default class Dealcategories extends Component {

  constructor(props) {
    super(props);
    this.navigateBackWithFilterdata = this.navigateBackWithFilterdata.bind(this);
    this.navigateBack = this.navigateBack.bind(this);
    this.state = {
        isLoading: true,
        dataSource: '',
    }
  }

  navigateBackWithFilterdata(cat_id){
    this.props.navigation.navigate('Deallist',{cat_id : cat_id});
  }

  _fetchData(callback) {
    DealsAPI.getDealCategories(callback); 
  }

  _handleConnectionChange = (isConnected) => {
    
    if(isConnected == false) {
      ToastAndroid.show("Please check your internet connection.",Toast.SHORT,Toast.TOP,FAILURE_TOAST_STYLE); 
    }
  };

  componentDidMount(){
    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
  } 

  componentWillMount(){

    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
    
    this._fetchData(responseJson => {
        let ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
        });
        const data = responseJson.data;
        this.setState({
            dataSource: ds.cloneWithRows(data),
            isLoading: false,
        });
    });
  }

  navigateBack() {
    this.props.navigation.goBack();
  }

  render() {
 
    return (
      <Container>
        {/* <AppHeader title="Filter Deals & Offers" navigation={this.props.navigation} showBack={true} showButtons={false} /> */}
        
        <Header androidStatusBarColor="#0b5864" iosBarStyle='light-content' noShadow={this.props.noShadow} backgroundColor="#0f7482" style={{backgroundColor: '#0f7482',}}>
          <Left>
            <Button transparent onPress={this.navigateBack}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Filter Deals & Offers</Title>
          </Body>
        </Header>


        <Content>
          <List>
            <ListItem style={styles.listItems} onPress={() => this.navigateBackWithFilterdata(0)} >
              <Text>All Deals & Offers</Text>
            </ListItem>
            <ListItem itemDivider style={styles.devider}>
              <Text style={{color:"#ffffff", padding: 0}}>By Category</Text>
            </ListItem>

            {this.state.isLoading ? 
              <Container>
                  <Content>
                      <Spinner color="#0f7482" />
                  </Content>
              </Container> :  
              <ListView
                  legacyImplementation={true}
                  enableEmptySections={ true }
                  automaticallyAdjustContentInsets={ false }
                  dataSource={this.state.dataSource}
                  renderRow={(rowData) =>
                      <ListItem style={styles.listItems} onPress={() => this.navigateBackWithFilterdata(rowData.id)} >
                        <Text>{rowData.name}</Text>
                      </ListItem>
                  }
              />
            }
            
          </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  devider:{
    backgroundColor: '#0f7482',
  }, 
  listItems:{
    paddingLeft:17,
    marginLeft:0,
    borderBottomWidth:1,
  },
  
})