import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Content, Text, Tab, Tabs, TabHeading } from 'native-base';
import Moment from 'moment';

import AppHeader from '../../components/Header';

import Newsdetails from '../../components/News/Newsdetails';
import Newsimage from '../../components/News/Newsimage';

export default class Newsdetail extends Component {
  render() {

    const { ID, post_title, post_content, thumbnail, post_date } = this.props.navigation.state.params.rowData;
    
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="News Details" showBack={true} showAccount={true} />
        <Content style={{backgroundColor: '#f0f0f0'}}>
          <View style={styles.newsTitleContainer}>
            <Text style={styles.newsTitle}>{post_title}</Text>
            <View style={styles.newsDateContainer}>
              <View style={styles.newsDateChild}><Text style={styles.newsAuthor}>{ Moment(post_date).format('D MMM YYYY')}</Text></View>
              {/* <View style={styles.newsDateChild}><Text style={styles.newsDate}>2 Days Ago . 5 Min Read</Text></View> */}
            </View>
          </View>
          <Newsimage thumbnail={thumbnail} />
          <Newsdetails post_content={post_content} />
        </Content>
      </Container>
    );
  }
}

const styles = {
  TabHeadingText:{
      color: '#ffffff',
  },
  TabHeadingFilterText:{
      color: '#ffffff',
  },
  TabHeading:{
      backgroundColor: '#0f7482',
  },
  TabHeadingFilter:{
      backgroundColor: '#f5ad2a'
  }, 
  newsTitleContainer: {
    padding: 14,
  }, 
  newsTitle: {
    color: '#575757', 
    fontSize: 20,
  }, 
  newsDate: {
    color: '#999999', 
    fontSize: 12,
    textAlign: 'right'
  }, 
  newsAuthor: {
    color: '#999999', 
    fontSize: 12,
    textAlign: 'left'
  }, 
  newsDateContainer:{
    flexDirection: 'row',
    paddingTop: 10
  }, 
  newsDateChild:{
    flex: 1
  }
}