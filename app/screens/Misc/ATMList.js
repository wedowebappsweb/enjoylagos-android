import React, { Component } from 'react'
import { Text, View, Image, Dimensions, ListView, StyleSheet, NetInfo, RefreshControl, AsyncStorage, ToastAndroid } from 'react-native'
import { Container, Content, List, ListItem, Left, Thumbnail, Body,  Card, CardItem, Right, Spinner } from 'native-base';

import MiscAPI from '../../utils/Misc';
import AppHeader from '../../components/Header';
import openMap from 'react-native-open-maps';
import Toast from 'react-native-toast-native';

import {
    Analytics,
    Hits as GAHits,
    Experiment as GAExperiment
  } from 'react-native-google-analytics';
  import DeviceInfo from 'react-native-device-info';
  var ga = this.ga = null;

class ATMList extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
        isLoading: true,
        isLocationReady: false,
        refreshing: false,
        dataSource: null,
        isConnected: 'offline',
        page:0,
        _data: null, 
        isLoadingMore: false,
        access_token: null, 
        
    }
  }

    _openMap(lat,lng) {
        openMap({ latitude: lat, longitude: lng });
    }

   
  _fetchData(callback) {  
    var data = {};
    navigator.geolocation.getCurrentPosition((position) => {
        var data ={
            lat : position.coords.latitude,
            lng : position.coords.longitude,
            page: this.state.page
        }
        
        MiscAPI.getINFOList(callback,data,"getAtms"); 
    }, (err) => {
        console.log(err.message);
    
    }, { enableHighAccuracy: true, timeout: 10000, maximumAge: 3600000 }); 
  }

    _fetchMore() {
        this.setState({ page: this.state.page + 1});
        this.setState({ isLoadingMore: false});
        this._fetchData(responseJson => {
            if(responseJson.status == 200){
                const data = this.state._data.concat(responseJson.data);
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(data),
                    isLoadingMore: false,
                    _data: data,
                });
            }else{
                this.setState({ isLoadingMore: false });
            }
        });
    }

  onRegionChange(region, lastLat, lastLong) {
    this.setState({
      isLoadingMore: true,
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
  }

  componentDidMount(){

    let clientId = DeviceInfo.getUniqueID();
    
    ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
    
    var screenView = new GAHits.ScreenView(
      'EnjoyLagos App',
      'ATM Screen',
      DeviceInfo.getReadableVersion(),
      DeviceInfo.getBundleId()
    );
    ga.send(screenView);

    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
    this._fetchData(responseJson => {
        
        let ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
        });
        const data = responseJson.data;
        this.setState({
            dataSource: ds.cloneWithRows(data),
            isLoading: false,
            _data:data
        });
    });
    
    AsyncStorage.getItem('user_token', (err, result) => {
        this.setState({access_token: result });
    });
    
  }

  _handleConnectionChange = (isConnected) => {
    if(isConnected == false) {
        ToastAndroid.show("Please check your internet connection.",Toast.SHORT); 
    }
  };

  componentWillMount(){

    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
   
  }

  _onRefresh() {
    console.log("HELLO WORLD -REFRESH");
  }

  _onEndReached() {
    this.setState({
        isLoadingMore: true
    });
    this._fetchMore();
  }

  render () {
    if (this.state.isLoading) {
        return (
            <Container>
                <AppHeader navigation={this.props.navigation} title="ATM Locations" showBack={true} showAccount={false} noShadow={true} />
                <Content>
                    <Spinner color="#0f7482" />
                </Content>
            </Container>
        );
    }
    
    return (
    <Container style={{ backgroundColor: '#f0f0f0', paddingBottom:10}}>
        <AppHeader navigation={this.props.navigation} title="ATM Locations" showBack={true} showAccount={false} noShadow={true} />
        <View style={{ backgroundColor: '#f0f0f0', paddingBottom:60, paddingTop :10 }}>
        <ListView
            legacyImplementation={true}
            style={styles.listItemBody}
            initialListSize={10}
            onEndReachedThreshold={100}
            pageSize={10}
            enableEmptySections={ true }
            automaticallyAdjustContentInsets={ false }
            dataSource={this.state.dataSource}

            renderRow={(rowData) =>
                <ListItem style={styles.listItem}>
                    <Card>
                        <CardItem style={{padding: 0, margin: 0}} button>
                            <Left>
                                {/* <Thumbnail square source={{uri: rowData.thumbnail }} /> */}
                                <Body>
                                    <Text style={styles.dealTitle}>{rowData.atm_branch_name}</Text>
                                    <Text style={styles.dealDesc} onPress={() => this._openMap(rowData.loc_lat,rowData.loc_lng)} >{rowData.address}</Text>
                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                </ListItem>
            }
            onEndReached={ () => this.setState({ isLoadingMore: true }, () => this._onEndReached()) }
            renderFooter={() => {
                return (
                this.state.isLoadingMore &&
                <View style={{ flex: 1, padding: 10 }}>
                    <Spinner color="#0f7482" />
                </View>
                );
            }}
        />
        </View>
       </Container>
    )
  }
}


const styles = {
    listItemBody:{
        backgroundColor: "#f0f0f0", 
        padding: 0, 
        margin:0, 
        borderWidth: 0
    },
    listItem: {
        backgroundColor: "#f0f0f0", 
        paddingBottom: 0, 
        marginBottom:0, 
        paddingTop: 0, 
        marginTop:0, 
        borderWidth:0,
        borderBottomWidth:0,
    },
    dealTitle:{
        color: '#0f7482',
        fontSize:16
    },
    dealDesc:{
        color: '#575757',
        fontSize:14
    },
    TabHeadingText:{
        color: '#ffffff',
    },
    TabHeadingFilterText:{
        color: '#ffffff',
    },
    TabHeading:{
        backgroundColor: '#0f7482',
    },
    TabHeadingFilter:{
        backgroundColor: '#f5ad2a'
    }
}

export default ATMList;