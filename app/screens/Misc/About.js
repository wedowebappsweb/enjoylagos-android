import React, { Component } from 'react';
import { View, StyleSheet, BackHandler, NetInfo, ToastAndroid } from 'react-native';
import { Container, Content, Text, Toast, Spinner } from 'native-base';
import HTMLView from 'react-native-htmlview';

import MiscAPI from '../../utils/Misc';
import AppHeader from '../../components/Header';

import {
  Analytics,
  Hits as GAHits,
  Experiment as GAExperiment
} from 'react-native-google-analytics';
import DeviceInfo from 'react-native-device-info';
var ga = this.ga = null;

export default class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading: true,
        URL: null,
        pageContent: ''
    }
  }

  getPageContent(callback){
    MiscAPI.getPageContent(callback,"about-us"); 
  }

  componentDidMount(){  

    let clientId = DeviceInfo.getUniqueID();
    
    ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
    
    var screenView = new GAHits.ScreenView(
      'EnjoyLagos App',
      'About Screen',
      DeviceInfo.getReadableVersion(),
      DeviceInfo.getBundleId()
    );
    ga.send(screenView);
    
    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);

    this.getPageContent(responseJson => {

        if(responseJson.status == 200){
          this.setState({
            isLoading: false,
            pageContent: responseJson.content
          });
        } else {
          this.setState({
            isLoading: false,
            pageContent: 'Page Not Found. Please try again.'
          });
        }
    });
    
  }

  _handleConnectionChange = (isConnected) => {
    if(isConnected == false) {
      ToastAndroid.show("Please check your internet connection.");
    }
  };

  componentWillMount(){

    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);

    BackHandler.addEventListener('hardwareBackPress', function() {
      // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
      // Typically you would use the navigator here to go to the last state.
      global.currentRoute = 'Home';
    });
    
  }
  render() {
    if (this.state.isLoading) {
      return (
          <Container style={{backgroundColor: '#ffffff'}}>
            <AppHeader navigation={this.props.navigation} title="About" showBack={false} showAccount={true} noShadow={true} />
                <Spinner color="#0f7482" />
          </Container>
      );
    }
    
    return (
      <Container style={{backgroundColor: '#ffffff'}}>
        <AppHeader navigation={this.props.navigation} title="About" showBack={false} showAccount={true} noShadow={true} />
        <Content>
          <View style={{padding:20}}>
          <HTMLView
            value={this.state.pageContent}
            stylesheet={styles}
          />
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  a: {
      fontWeight: '300',
      color:'blue',
      borderBottomWidth:1
  }
});