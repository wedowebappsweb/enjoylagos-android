import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, ListView, NetInfo, Dimensions, ToastAndroid } from 'react-native'
import { Container, Content, List, ListItem, Left, Thumbnail, Body, Card, CardItem, Right, Spinner } from 'native-base';
import HTMLView from 'react-native-htmlview';

import AppHeader from '../../components/Header';
import LagosInfoHeader from '../../components/misc/LagosInfoHeader';
import LagosSos from '../../components/misc/LagosSos';
import renderIf from '../../functions/renderif';
import Toast from 'react-native-toast-native';


import NewsAPI from '../../utils/News';
import {
    Analytics,
    Hits as GAHits,
    Experiment as GAExperiment
  } from 'react-native-google-analytics';
  import DeviceInfo from 'react-native-device-info';
  var ga = this.ga = null;
export default class Lagosinfo extends Component {

  constructor(props) {
    super(props);
    this.navigateNewsDetails = this.navigateNewsDetails.bind(this);
    this.handleListScroll = this.handleListScroll.bind(this);
    this.handleScroll = this.handleScroll.bind(this);

    var {height, width} = Dimensions.get('window');

    this.offset = 0;
    this.offsetList = 0;

    this.state = {
      isScrollEnabled: true, 
      isListScrollEnabled: false, 
      isLoading: true,
      dataSource: null,
      isConnected: 'offline',
      isLoadingMore: false,
      page: 0,
      _data: null,
      windowH : height,
      _dataEmpty: false
    }
  }

  handleScroll(event){
    const curOffset = event.nativeEvent.contentOffset.y;

    var direction = curOffset > this.offset ? 'down' : 'up';
    this.offset = curOffset;
    if( direction == 'up' && curOffset < 330 ){
        this.setState({
          isScrollEnabled: true,
          isListScrollEnabled: false
        });
        return false;
    }

    if(event.nativeEvent.contentOffset.y > 330  && this.state.isScrollEnabled && direction == 'down'){
      this.setState({
        isScrollEnabled: false,
        isListScrollEnabled: true
      });
    }
  }

  handleListScroll(event){
    const curOffset = event.nativeEvent.contentOffset.y;

    var direction = curOffset > this.offsetList ? 'down' : 'up';
    this.offsetList = curOffset
    if( direction == 'up' && curOffset <= 40 && this.state.isListScrollEnabled ){
        this.setState({
            isScrollEnabled: true, 
            isListScrollEnabled: false
        });
    }
    return false;

    if(event.nativeEvent.contentOffset.y < 50 && this.state.isListScrollEnabled){
      this.setState({
        isScrollEnabled: true,
        isListScrollEnabled: true
      });
    }
  }

  _fetchData(callback) {
    var data ={
        page: this.state.page
    }
    
    NewsAPI.getNewsList(callback,data);
  }

  _onEndReached() {
      this._fetchMore();
  }

  _fetchMore() {
      this.setState({ page: this.state.page + 1});
      
      this._fetchData(responseJson => {
          
          if(responseJson.status == 200){
              const data = this.state._data.concat(responseJson.data);
              this.setState({
                  dataSource: this.state.dataSource.cloneWithRows(data),
                  isLoadingMore: false,
                  _data: data,
              });
          }else{
              this.setState({isLoadingMore: false});
          }
      });
  }

  componentDidMount(){   
      
    let clientId = DeviceInfo.getUniqueID();
    
    ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
    
    var screenView = new GAHits.ScreenView(
      'EnjoyLagos App',
      'LagosInfo Screen',
      DeviceInfo.getReadableVersion(),
      DeviceInfo.getBundleId()
    );
    ga.send(screenView);

    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
    this._fetchData(responseJson => {
        let ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
        });
        const data = responseJson.data;
        
            if ( responseJson.status == 200) {
                this.setState({
                    dataSource: ds.cloneWithRows(data),
                    isLoading: false,
                    _data:data,
                    page: this.state.page + 1
                });
            } else {
                this.setState({
                    isLoading: false,
                    _dataEmpty: true
                });
            }
    });
  }

  _handleConnectionChange = (isConnected) => {
    
    if(isConnected == false) {
      
        ToastAndroid.show("Please check your internet connection.",Toast.SHORT); 
      
    }
  };

  componentWillMount(){

    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
   
  }
  navigateNewsDetails(rowData) {
    this.props.navigation.navigate('Newsdetail', {rowData});
  }

  render() {
    var {height, width} = Dimensions.get('window');
    var maxlimit = 100;

    if (this.state._dataEmpty) {
        return (
            <Container>
                <AppHeader navigation={this.props.navigation} title="Lagos Info" showBack={false} showAccount={true} noShadow={true} />
                <Content scrollEnabled={this.state.isScrollEnabled} bounces={false} onScroll={this.handleScroll} >
                <LagosInfoHeader/>
                <View style={{ backgroundColor: '#f0f0f0'}}>
                <LagosSos test='testing' navigation={this.props.navigation} />
                <Text style={{fontSize: 24, color: '#575757', paddingLeft: 17}}>Latest News</Text>
                </View>
                    <View style={{ alignItems:'center', justifyContent:'center'}}>
                    <Text style={{color: '#0f7482', textAlign:'center',paddingTop: 10, fontSize:16 }}>No Posts Found.</Text>
                    </View>
                </Content>
            </Container>
        );
    }

    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Lagos Info" showBack={false} showAccount={true} noShadow={true} />
        <Content scrollEnabled={this.state.isScrollEnabled} alwaysBounceHorizontal={false} alwaysBounceVertical={false} bounces={false} onScroll={this.handleScroll} >
          <LagosInfoHeader/>
          <View style={{ backgroundColor: '#f0f0f0'}}>
          <LagosSos test='testing' navigation={this.props.navigation} />
          <Text style={{fontSize: 24, color: '#575757', paddingLeft: 17}}>Latest News</Text>
          
          {/* <LatestNewsList navigation={this.props.navigation}/>  */}
          
          <View style={{flex:1}}>
          {this.state.isLoading ? 
            
            <Spinner color="#0f7482" /> : 
            
            <ListView
              legacyImplementation={true}
              style={styles.listItemBody}
              style={{height: height-90}}
              initialListSize={10}
              onEndReachedThreshold={100}
              pageSize={10}
              enableEmptySections={ true }
              automaticallyAdjustContentInsets={ false }
              dataSource={this.state.dataSource}
              scrollEnabled={this.state.isListScrollEnabled}
              renderRow={(rowData) =>
                  <ListItem style={styles.listItem}>
                      <Card>
                          <CardItem style={{padding: 0, margin: 0}} button onPress={() => this.navigateNewsDetails(rowData)}>
                              <Left>
                                  <Thumbnail square source={{ uri: rowData.thumb }} />
                                  <Body>
                                      <Text style={styles.dealTitle}>{rowData.post_title}</Text>
                                      <HTMLView
                                          value={ ((rowData.post_content).length > maxlimit) ? 
                                              (((rowData.post_content).substring(0,maxlimit-3)) + '...') : 
                                              rowData.post_content }
                                      />
                                  </Body>
                              </Left>
                          </CardItem>
                      </Card>
                  </ListItem>
              }
              onEndReached={ () => this.setState({ isLoadingMore: true }, () => this._onEndReached()) }
              onScroll={this.handleListScroll}
              renderFooter={() => {
                  return (
                  this.state.isLoadingMore &&
                  <View style={{ flex: 1, padding: 10 }}>
                      <Spinner color="#0f7482" />
                  </View>
                  );
              }}
            />
          }
          </View>
          </View> 
        </Content>
      </Container>
    );
  }
}

const styles = {
  listItemBody:{
      backgroundColor: "#f0f0f0", 
      padding: 0, 
      margin:0, 
      borderWidth: 0,
  },
  listItem: {
      backgroundColor: "#f0f0f0", 
      paddingBottom: 0, 
      marginBottom:0, 
      paddingTop: 0, 
      marginTop:0, 
      borderWidth:0,
      borderBottomWidth:0,
  },
  dealTitle:{
      color: '#0f7482',
      fontSize:16
  },
  dealDesc:{
      color: '#575757',
      fontSize:14
  },
  TabHeadingText:{
      color: '#ffffff',
  },
  TabHeadingFilterText:{
      color: '#ffffff',
  },
  TabHeading:{
      backgroundColor: '#0f7482',
  },
  TabHeadingFilter:{
      backgroundColor: '#f5ad2a'
  }
}