import React, { Component } from 'react';
import { View, FlatList, ListView, Modal, NetInfo, BackHandler, TouchableOpacity, StyleSheet, Image, Dimensions, ToastAndroid } from 'react-native';
import { Container, Content, Text, Spinner, ListItem, Thumbnail, Button, Icon } from 'native-base';
import {CachedImage} from "react-native-img-cache";
import MiscAPI from '../../utils/Misc';
import AppHeader from '../../components/Header';
import Toast from 'react-native-toast-native';


import {
  Analytics,
  Hits as GAHits,
  Experiment as GAExperiment
} from 'react-native-google-analytics';
import DeviceInfo from 'react-native-device-info';
var ga = this.ga = null;

const loading = require('../../components/img/loading.gif')
const { width, height } = Dimensions.get('window');
const equalWidth =  (width / 3 ) 

export default class Gallery extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
        isLoading: true,
        URL: null,
        moviesList: [],
        modalVisible:false,
        modalIMAGE: '',
        imageLoading: false,
    }
  }

  setModalVisible(visible) {
      this.setState({modalVisible: visible});
  }

  _onLoad = () => {
    this.setState(() => ({ imageLoading: true }))
  }

  _onLoadEnd = () => {
    this.setState(() => ({ imageLoading: false }))
  }
  // _toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible })
  _keyExtractor = (item, index) => item.id;

  getMoviesFromApiAsync = () => {

    return fetch(APIURL + 'getGalleryImages', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body:''
      })
      .then((response) => response.json())
      .then((responseJson) => {
       
        this.setState({ 
          moviesList: responseJson.images,
          isLoading: false
        }) // this will update state to re-render ui
        return responseJson.images;
      })
      .catch((error) => {
        console.error(error);
      });
  }


  getFrame(callback){
    MiscAPI.getFrame(callback,"gallery"); 
  }

  renderRowItem = (itemData) => {
    return (
      <View>
        <View> 
        <TouchableOpacity onPress={() =>
                        this.setState({ modalIMAGE: itemData.item.image, modalVisible:true })}>
          {/* <Image style={{ height: 150,  width : equalWidth}} source={{ uri: itemData.item.thumb }} resizeMode='cover' />  */}
          <CachedImage style={{ height: equalWidth,  width : equalWidth}} source={{ uri: itemData.item.thumb }} resizeMode='cover' mutable />
        </TouchableOpacity>
        </View>
        
      </View>
    )
  }

  componentWillMount() {

    let clientId = DeviceInfo.getUniqueID();
    
    ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
    
    var screenView = new GAHits.ScreenView(
      'EnjoyLagos App',
      'Gallery Screen',
      DeviceInfo.getReadableVersion(),
      DeviceInfo.getBundleId()
    );
    ga.send(screenView);

    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
    {this.getMoviesFromApiAsync()}
  }

  _handleConnectionChange = (isConnected) => {
    if(isConnected == false) {
      ToastAndroid.show("Please check your internet connection.",Toast.SHORT); 
    }
  };

  componentDidMount(){
    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);

    BackHandler.addEventListener('hardwareBackPress', function() {
      // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
      // Typically you would use the navigator here to go to the last state.
      global.currentRoute = 'Home';
    });
  } 


  render() {
    if (this.state.isLoading) {
      return (
          <Container>
            <AppHeader navigation={this.props.navigation} title="Gallery" showBack={false} showAccount={true} noShadow={true} />
                <Spinner color="#0f7482" />
          </Container>
      );
    }

    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Gallery" showBack={false} showAccount={true} noShadow={true} />
        
        <FlatList
          data={this.state.moviesList}
          numColumns={3}
          keyExtractor={this._keyExtractor}
          renderItem={this.renderRowItem}
        />

            <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
                  this.setModalVisible(!this.state.modalVisible)
                  }}
            >
              
              <View style={{ flex:1, justifyContent: 'center', padding: 10, backgroundColor: 'rgba(0, 0, 0, 0.9)' }}>
              <Button style={{ paddingTop: 20 }} transparent onPress={() => {
                  this.setModalVisible(!this.state.modalVisible)
                  }}>
                  <Text style={{ color:'#ffffff'}}>CLOSE</Text>
              </Button>
                  <View style={{ flex:1 }}>
                     <Image style={{ flex:1, resizeMethod: 'scaled' }} source={{ uri: this.state.modalIMAGE }} resizeMode='contain'  onLoadStart={() => this._onLoad()}
                      onLoadEnd={() => this._onLoadEnd()} />

                      {
                        this.state.imageLoading &&
                        <View style={styles.loadingView}>
                          <Spinner color="#ffffff" />
                        </View>
                      }

                  </View>
              </View>
            </Modal>
            
      </Container>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    flexDirection: 'column'
  },
  loadingView: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,.5)'
  },
  loadingImage: {
    width: 60,
    height: 60,
  }
});