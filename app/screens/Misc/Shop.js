import React, { Component } from 'react';
import { View, WebView, BackHandler, NetInfo, ToastAndroid } from 'react-native';
import { Container, Content, Text, Spinner } from 'native-base';

import MiscAPI from '../../utils/Misc';
import AppHeader from '../../components/Header';
import Toast from 'react-native-toast-native';

import {
  Analytics,
  Hits as GAHits,
  Experiment as GAExperiment
} from 'react-native-google-analytics';
import DeviceInfo from 'react-native-device-info';
var ga = this.ga = null;
export default class Shop extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
        isLoading: true,
        URL: null,
    }
  }

  getFrame(callback){
    MiscAPI.getFrame(callback,"shop"); 
  }
  componentDidMount(){  
    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
    this.getFrame(responseJson => {
        
        this.setState({
            URL: responseJson.url,
            isLoading: false,
        });
    });
  }

  _handleConnectionChange = (isConnected) => {
    if(isConnected == false) {
      ToastAndroid.show("Please check your internet connection.",Toast.SHORT,Toast.TOP,FAILURE_TOAST_STYLE); 
    }
  };

  componentWillMount(){
    let clientId = DeviceInfo.getUniqueID();
    
    ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
    
    var screenView = new GAHits.ScreenView(
      'EnjoyLagos App',
      'HotelBooking Screen',
      DeviceInfo.getReadableVersion(),
      DeviceInfo.getBundleId()
    );
    ga.send(screenView);
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);

    BackHandler.addEventListener('hardwareBackPress', function() {
      // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
      // Typically you would use the navigator here to go to the last state.
      global.currentRoute = 'Home';
    });
  }

  ActivityIndicatorLoadingView() {
    
    return (
      <Container>
        <Spinner color="#0f7482" />
      </Container>
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
          <Container>
            <AppHeader navigation={this.props.navigation} title="Hotel Booking" showBack={false} showAccount={true} noShadow={true} />
                <Spinner color="#0f7482" />
          </Container>
      );
  }

    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Hotel Booking" showBack={false} showAccount={true} noShadow={true} />
        
        <WebView
        source={{uri: this.state.URL}}
        renderLoading={this.ActivityIndicatorLoadingView} 
        startInLoadingState={true}
        />
      </Container>
    );
  }
}