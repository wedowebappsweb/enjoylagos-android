import React, { Component } from 'react';
import { AsyncStorage, View, ListView, StyleSheet } from 'react-native';
import { Container, Content, Text, ListItem, List, Card, CardItem, Left, Thumbnail, Body, Spinner, Tab, Tabs, TabHeading } from 'native-base';

import MiscAPI from '../../utils/Misc';

import AppHeader from '../../components/Header';

export default class Transaction extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            refreshing: false,
            dataSource: null,
            page:0,
            _data: null, 
            isLoadingMore: false,
            access_token: null, 
            empty: false
        }
    }
    
    
    _fetchData(callback) {
        var data = {};
        var data ={
            page: this.state.page,
        }
        AsyncStorage.getItem('user_token', (err, result) => {
            MiscAPI.getTransactionHistory(callback,data,result) 
        });
    }

    _fetchMore() {
        if(this.state.count == 10){
            this.setState({ page: this.state.page + 1});
            this.setState({ isLoadingMore: true});
            this._fetchData(responseJson => {
                if(responseJson.status == 200){
                    const data = this.state._data.concat(responseJson.data);
                    this.setState({
                        dataSource: this.state.dataSource.cloneWithRows(data),
                        isLoadingMore: false,
                        _data: data,
                        count: responseJson.count
                    });
                }else{
                    this.setState({ isLoadingMore: false });
                    this.setState({ page: this.state.page - 1});
                }
            });
        }
    }

    componentDidMount() {
        
        this._fetchData(responseJson => {
            
            if(responseJson.status == 200 && responseJson.data.length > 0) {
                
                let ds = new ListView.DataSource({
                    rowHasChanged: (r1, r2) => r1 !== r2,
                });
                
                let data = responseJson.data;
                
                this.setState({
                    dataSource: ds.cloneWithRows(data),
                    isLoading: false,
                    _data:data,
                    empty: false,
                    count: responseJson.count,
                });
            } else {
                this.setState({
                    isLoading: false,
                    empty: true,
                });
            }
        });
    }

    _onRefresh() {
    }

    _onEndReached() {
        this._fetchMore();
    }

    render() {
        
        if (this.state.isLoading) {
            return (
                <Container>
                    <AppHeader navigation={this.props.navigation} title="All Transactions" showBack={true} />
                    <Content>
                        <Spinner color="#0f7482" />
                    </Content>
                </Container>
            );
        }

        if (this.state.empty) {
            return (
                <Container>
                    <AppHeader navigation={this.props.navigation} title="All Transactions" showBack={true} manageAccount={true} />
                    <Content>
                        <View style={{ alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color: '#0f7482', textAlign:'center',paddingTop: 10, fontSize:16 }}>You’ve not made any transaction.</Text>
                        </View>
                    </Content>
                </Container>
            );
        }

        return (
            <Container style={{backgroundColor: '#f0f0f0'}}>
            <AppHeader navigation={this.props.navigation} title="All Transactions" showBack={true} manageAccount={'Wallet'} />
            <View style={{ paddingBottom:60, paddingTop :10 }} >
            <ListView
                legacyImplementation={true}
                // style={styles.listItemBody}
                initialListSize={10}
                onEndReachedThreshold={100}
                pageSize={10}
                enableEmptySections={ true }
                automaticallyAdjustContentInsets={ false }
                dataSource={this.state.dataSource}

                renderRow={(rowData) =>
                    <ListItem style={styles.listItem}>
                        <Card>
                            <CardItem style={{padding: 0, margin: 0}} button>
                                <Left>
                                    <Body>
                                        <Text style={styles.title}> ₦{rowData.amount}</Text>
                                        <Text style={styles.desc}>{rowData.message}</Text>
                                    </Body>
                                </Left>
                            </CardItem>
                        </Card>
                    </ListItem>
                }
                onEndReached={ () => this._onEndReached() }
                renderFooter={() => {
                    return (
                    this.state.isLoadingMore &&
                    <View style={{ flex: 1, padding: 10 }}>
                        <Spinner color="#0f7482" />
                    </View>
                    );
                }}
            />
            </View>
            </Container>
        );
    }
}

const styles = {
    title:{
        color: '#0f7482',
        fontSize:16
    },
    desc:{
        color: '#575757',
        fontSize:14
    },
    listItemBody:{
        paddingTop: 10,
        padding: 0, 
        margin:0, 
        borderWidth: 0
    },
    listItem: {
        paddingBottom: 0, 
        marginBottom:0, 
        paddingTop: 0, 
        marginTop:0, 
        borderWidth:0,
        borderBottomWidth:0,
    },
   
}