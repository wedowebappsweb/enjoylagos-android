import React, { Component } from 'react';
import { View, WebView, BackHandler, NetInfo, ToastAndroid } from 'react-native';
import { Container, Content, Toast, Text, Spinner } from 'native-base';

import MiscAPI from '../../utils/Misc';
import AppHeader from '../../components/Header';

import {
  Analytics,
  Hits as GAHits,
  Experiment as GAExperiment
} from 'react-native-google-analytics';
import DeviceInfo from 'react-native-device-info';
var ga = this.ga = null;

export default class FlightBooking extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
        isLoading: true,
        URL: null,
    }
  }

  getFrame(callback){
    MiscAPI.getFrame(callback,"flight_book"); 
  }

  componentDidMount(){  

    let clientId = DeviceInfo.getUniqueID();
    
    ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
    
    var screenView = new GAHits.ScreenView(
      'EnjoyLagos App',
      'FlightBooking Screen',
      DeviceInfo.getReadableVersion(),
      DeviceInfo.getBundleId()
    );
    ga.send(screenView);

    NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
    this.getFrame(responseJson => {
        
        this.setState({
            URL: responseJson.url,
            isLoading: false,
        });
    });

    BackHandler.addEventListener('hardwareBackPress', function() {
        global.currentRoute = 'Home';
    });
    
  }

  _handleConnectionChange = (isConnected) => {
    if(isConnected == false) {
      ToastAndroid.show('Please check your internet connecion.');
    }
  };

  componentWillMount(){
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
  }

  ActivityIndicatorLoadingView() {
    return (
      <Container>
        <Spinner color="#0f7482" />
      </Container>
    );
  }

  render() {
    
    if (this.state.isLoading) {
      return (
          <Container>
            <AppHeader navigation={this.props.navigation} title="Flight Booking" showBack={false} showAccount={true} noShadow={true} />
                <Spinner color="#0f7482" />
          </Container>
      );
    }

    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Flight Booking" showBack={false} showAccount={true} noShadow={true} />
        
        <WebView
        source={{uri: this.state.URL}}
        renderLoading={this.ActivityIndicatorLoadingView} 
        startInLoadingState={true}
        />
      </Container>
    );
  }
}