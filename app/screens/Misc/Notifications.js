import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Content, Text } from 'native-base';

import AppHeader from '../../components/Header';
import Notificationlist from '../../components/misc/Notificationlist';

export default class Notifications extends Component {
  render() {
    return (
      <Container>
        <AppHeader navigation={this.props.navigation} title="Notifications" showBack={true} showAccount={true} />
        <Content>
          <Notificationlist navigation={this.props.navigation} />
        </Content>
      </Container>
    );
  }
}