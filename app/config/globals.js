global.BASEURL = 'http://enjoylagos.net/';
// global.BASEURL = 'http://enjoylagos.wedowebapps.in/';
global.APIURL = BASEURL+'wp-json/api/';
global.AUTHURL = BASEURL+'wp-json/auth/';
global.GOOGLE_ANALYTICS = 'UA-114719452-1';

global.ToastTextFontSize = 18;
global.vendorScreen = false;
global.bookTour = false;

global.SUCCESS_TOAST_STYLE = {
    backgroundColor: "#82b625",
    width: 300,
    height: 130,
    paddingLeft: 30,
    paddingRight: 30,
    color: "#ffffff",
    fontSize: 16,
    lineHeight: 2,
    lines: 4,
    borderRadius: 15,
    fontWeight: "bold",
    yOffset: 40,
}

global.FAILURE_TOAST_STYLE = {
    backgroundColor: "#df2200",
    width: 300,
    height: 130,
    color: "#ffffff",
    fontSize: 16,
    paddingLeft: 30,
    paddingRight: 30,
    lineHeight: 0,
    lines: 4,
    borderRadius: 15,
    fontWeight: "bold",
    yOffset: 40
}