import React, { Component } from 'react';
import { TabNavigator, StackNavigator, DrawerNavigator } from 'react-navigation';

import './globals.js';

import Home from '../screens/Home';

import Deallist from '../screens/Deals/Deallist';
import Dealdetails from '../screens/Deals/Dealdetails';
import Dealcategories from '../screens/Deals/Dealcategories';
import VendorDetails from '../screens/Deals/VendorDetails';

import Tourlist from '../screens/Tour/Tourlist';
import Tourdetail from '../screens/Tour/Tourdetail';


import Gallery from '../screens/Misc/Gallery';
import Events from '../screens/Misc/Events';
import Shop from '../screens/Misc/Shop';
import FlightBooking from '../screens/Misc/FlightBooking';
import About from '../screens/Misc/About';
import Lagosinfo from '../screens/Misc/Lagosinfo';
import Notifications from '../screens/Misc/Notifications';
import ATMList from '../screens/Misc/ATMList';
import PoliceStationList from '../screens/Misc/PoliceStationList';
import EmergencyNumberList from '../screens/Misc/EmergencyNumberList';

import Newsdetail from '../screens/News/Newsdetail';
import Newslist from '../screens/News/Newslist';

import Login from '../screens/User/Login';
import Register from '../screens/User/Register';
import Account from '../screens/User/Account';
import ManageAccount from '../screens/User/ManageAccount';

import Transaction from '../screens/Misc/Transaction';

import ControlPanel from '../components/ControlPanel';

export const Deals = StackNavigator({
    Deallist: {
        screen : Deallist,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    }, 
    
    Dealdetails: {
        screen : Dealdetails,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    }, 
    Dealcategories: {
        screen : Dealcategories,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    VendorDetails: {
        screen : VendorDetails,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    DealLogin: {
        screen : Account,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    
     
});

export const Lagostour = StackNavigator({
    Tourlist: {
        screen : Tourlist,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    }, 
    Tourdetail: {
        screen : Tourdetail,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    }, 
    TourLogin: {
        screen : Account,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    Home: {
        screen : Home,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
});

export const News = StackNavigator({
    Newsdetail: {
        screen : Newsdetail,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
});

export const NotificationStack = StackNavigator({
    Home: {
        screen : Home,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    NotificationList: {
        screen : Notifications,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    },
    Dealdetails: {
        screen : Dealdetails,
        navigationOptions: ({ navigation }) => ({
            header: null
        }),
    }, 
});

export const User = StackNavigator({
    Account: {
        screen: Account,
        navigationOptions: ({
            header: null
        })
    }
});

export const Manage = StackNavigator({
    ManageAccount: {
        screen: ManageAccount,
        navigationOptions: ({
            header: null
        })
    }
})

export const LagosInfoStack = StackNavigator({
    Lagosinfo : {
        screen : Lagosinfo,
        navigationOptions: ({
            header: null
        }),
    }, 
    Newsdetail : {
        screen: Newsdetail,
        navigationOptions: ({
            header: null
        })
    },
    ATMList:{
        screen:ATMList,
        navigationOptions:({
            header:null
        })
    },
    PoliceStationList:{
        screen:PoliceStationList,
        navigationOptions:({
            header:null
        })
    },
    EmergencyNumberList:{
        screen:EmergencyNumberList,
        navigationOptions:({
            header:null
        })
    }
});

export const AppRoot = DrawerNavigator({
    Home: {
        screen: Home,
    },
    Deals: {
        screen: Deals,
    },
    Lagostour: {
        screen: Lagostour,
    },
    LagosInfo: {
        screen: LagosInfoStack,
    },
    Gallery:{
        screen: Gallery,
    },
    Events:{
        screen: Events,
    },
    Shop:{
        screen: Shop,
    },
    FlightBooking:{
        screen: FlightBooking,
    },
    About:{
        screen: About,
    }, 
    Notifications:{
        screen: NotificationStack
    },
    Account: {
        screen: Account
    },
    ManageAccount: {
        screen: Manage
    },
    Transaction: {
        screen: Transaction
    }
    
}, {
    contentComponent: ControlPanel,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
});