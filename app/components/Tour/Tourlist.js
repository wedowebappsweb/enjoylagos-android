import React, { Component } from 'react'
import { Text, View, Image, ListView, BackHandler, Dimensions, NetInfo, StyleSheet, RefreshControl, ToastAndroid} from 'react-native'
import { Container, Content, Card, CardItem, Left, Right, Thumbnail, Body, Tex, Icon, ListItem, Spinner } from 'native-base';
import {CachedImage} from "react-native-img-cache";
import ToursAPI from '../../utils/Tours';
import Toast from 'react-native-toast-native';

import {
    Analytics,
    Hits as GAHits,
    Experiment as GAExperiment
  } from 'react-native-google-analytics';
  import DeviceInfo from 'react-native-device-info';
  var ga = this.ga = null;
  
class Tourlist extends Component {

    constructor(props) {
        super(props);
        this.navigateTourDetails = this.navigateTourDetails.bind(this);

        this.state = {
            isLoading: true,
            refreshing: false,
            dataSource: null,
            isLoadingMore: false,
            isConnected: 'offline',
            page: 0,
            _data: null,
        }

    }

    navigateTourDetails = (rowData) => {
        this.props.navigation.navigate('Tourdetail',{rowData});
    }
    
    _fetchData(callback) {
        var data = {};
        navigator.geolocation.getCurrentPosition((position) => {
            var data ={
                lat : position.coords.latitude,
                lng : position.coords.longitude,
                sortby : this.props.sortBy,
                hotdeals: this.props.hotdeals,
                page: this.state.page
            }
            if(this.props.params && this.props.params.cat_id != null){
                data.cat_id = this.props.params.cat_id;
            }
            ToursAPI.getToursList(callback,data);
        }, (err) => {
            console.log(err.message);
        
        }, { enableHighAccuracy: false, timeout: 10000, maximumAge: 3600000 }); 
        
    }

    _fetchMore() {
        this.setState({ page: this.state.page + 1});
        this._fetchData(responseJson => {
            if(responseJson.status == 200){
                const data = this.state._data.concat(responseJson.data);
                
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(data),
                    isLoadingMore: false,
                    _data: data,
                });
            }else{
                this.setState({isLoadingMore: false});
            }
        });
    }

    componentDidMount() {

        let clientId = DeviceInfo.getUniqueID();
        
        ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
        
        var screenView = new GAHits.ScreenView(
          'EnjoyLagos App',
          'Tour Screen',
          DeviceInfo.getReadableVersion(),
          DeviceInfo.getBundleId()
        );
        ga.send(screenView);

        NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
        this._fetchData(responseJson => {
            let ds = new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2,
            });
            const data = responseJson.data;
            this.setState({
                dataSource: ds.cloneWithRows(data),
                isLoading: false,
                _data:data
            });
        });

        BackHandler.addEventListener('hardwareBackPress', function() {
            // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
            // Typically you would use the navigator here to go to the last state.
            
            global.currentRoute = 'Home';
        });
    }

    _handleConnectionChange = (isConnected) => {
        if(isConnected == false) {

            ToastAndroid.show("Please check your internet connection.",Toast.SHORT); 
          
        }
    };

    componentWillMount(){
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
    }

    _onRefresh() {
        console.log("HELLO WORLD -REFRESH");
    }

    _onEndReached() {
        
        this._fetchMore();
    }

    render () {

        if (this.state.isLoading) {
            return (
                <Container>
                    <Content>
                        <Spinner color="#0f7482" />
                    </Content>
                </Container>
            );
        }

        return (
            <Container style={{paddingTop: 10, paddingBottom: 10, backgroundColor: '#f0f0f0'}}>
                <ListView
                    legacyImplementation={true}
                    style={styles.listItemBody}
                    initialListSize={10}
                    onEndReachedThreshold={100}
                    pageSize={10}
                    enableEmptySections={ true }
                    automaticallyAdjustContentInsets={ false }
                    dataSource={this.state.dataSource}

                    renderRow={(rowData) =>
                        <ListItem style={styles.listItem}>
                            <Card style={{flexWrap:'nowrap'}}>
                                <CardItem button onPress={() => this.navigateTourDetails(rowData)} cardBody>
                                    <CachedImage resizeMode='cover' source={{uri: rowData.thumbnail }} style={{height: 140, flex: 1}}/>
                                </CardItem>
                                <CardItem button onPress={() => this.navigateTourDetails(rowData)} button onPress={this.navigateDealDetails}>
                                    <Body>
                                        <Text style={styles.dealTitle}>{rowData.post_title}</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </ListItem>
                    }
                    onEndReached={ () => this.setState({ isLoadingMore: true }, () => this._onEndReached()) }
                    renderFooter={() => {
                        return (
                        this.state.isLoadingMore &&
                        <View style={{ flex: 1, padding: 10 }}>
                            <Spinner color="#0f7482" />
                        </View>
                        );
                    }}
                />
            </Container>
        )
    }
}


const styles = {
    listItemBody:{
        backgroundColor: "#f0f0f0", 
        padding: 0, 
        margin:0, 
        borderWidth: 0
    },
    listItem: {
        backgroundColor: "#f0f0f0", 
        paddingBottom: 0, 
        marginBottom:0, 
        paddingTop: 0, 
        marginTop:0, 
        borderWidth:0,
        borderBottomWidth:0,
    },
    dealTitle: {
        color: '#0f7482',
        fontSize:14,
    }, 
    icon : {
        color: '#0f7482',
    }
}

export default Tourlist;