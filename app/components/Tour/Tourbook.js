import React, { Component } from 'react'
import { Picker, Text, View, Image, Dimensions, StyleSheet,NetInfo, Modal, AsyncStorage, ToastAndroid } from 'react-native'
import { Container, Content, Card, CardItem, Left, Thumbnail, Body, Icon, Form, Item, Label, Input, Button, Spinner, ActionSheet} from 'native-base';

import DatePicker from 'react-native-datepicker';
import PhoneInput from 'react-native-phone-input';
import Toast from 'react-native-toast-native';

var PickerItem = Picker.Item;

class Tourdetails extends Component {

    constructor(props){
        super(props)
        this.state = {
            isLoading: false,
            date:null,
            no_travellers: null,
            access_token:null,
            today: new Date(),
            phone: '',
            modalVisible: false,
            verifyModalVisible : false,
            verifyLoading: false
        }
    }

    componentDidMount(){
        NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
        this.setState({
            date: new Date()
        })
        AsyncStorage.getItem('userData', (err, result) => {
            if(result) {
                let user_data = [];

                user_data = JSON.parse(result);
                
                this.setState({ 
                    access_token: user_data.token,
                    phone: user_data.phone
                });  
                global.access_token = user_data.token;
            }
        });
    }

    componentWillUnmount() {
        // Toast.toastInstance = null;
        // ActionSheet.actionsheetInstance = null;
    }

    _handleConnectionChange = (isConnected) => {
        if(isConnected == false) {
            
            ToastAndroid.show("Please check your internet connection.",Toast.SHORT,Toast.TOP,FAILURE_TOAST_STYLE); 
            
        }
    };

    componentWillMount(){
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
    }

  render () {

    return (
        <Container style={{padding: 10, backgroundColor: '#f0f0f0'}}>
            <Content style={{ }}>
                <Form>
                    <Item>
                        <Label>Trip Date</Label>
                        <DatePicker
                        ref="datepicker"
                        style={{flex: 1, borderWidth: 0 }}
                        date={this.state.date}
                        mode="date"
                        placeholder="Trip Date"
                        format="YYYY-MM-DD"
                        minDate={this.state.today}
                        maxDate="2050-06-01"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        showIcon= {false}
                        customStyles={{dateInput:{borderWidth: 0, paddingRight:25}}}
                        onDateChange={(date) => {this.setState({date: date})}}
                    />
                    </Item>
                    <Item>
                        <Label>No. of Travellers</Label>
                        <Input  keyboardType='numeric' style={{ paddingLeft:65 }} value={this.state.no_travellers}  onChangeText={(text)=> this.onChanged(text)}/>
                    </Item>
                    <Item>
                        <View style={{flexDirection:"row", paddingTop: 10, marginTop:10, paddingBottom:10, }}>
                            <View style={{flex:1.4, flexDirection: "row"}}>
                                <Label>Phone</Label>
                            </View>
                            <View style={{flex:3}}>
                                <PhoneInput
                                    ref={(ref) => { this.phone = ref; }}
                                    onPressFlag={this.onPressFlag}
                                    onChangePhoneNumber= {(number) => this.setState({phone: number})}
                                    offset={20}
                                    value={this.state.phone}
                                    initialCountry='ng'
                                />
                            </View>
                        </View>
                    </Item>
                    <View style={{ padding: 16 }}>
                        { this.state.isLoading ? <Spinner color="#0f7482" /> :
                        <Button onPress={this.bookTour} full success style={{ backgroundColor : '#0f7482' }} >
                            <Text style={{ color: "#ffffff" }}>{ this.state.access_token != null ? "Book" : "Login To Book" }</Text>
                        </Button>
                        }
                    </View>
                </Form>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        // alert('Modal has been closed.');
                        this.setState({ modalVisible: false})
                    }}>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                        <View style={{ padding: 30, backgroundColor: '#fff' }}>
                            <Form>
                                {/* <Text style={{color: 'red', fontSize: 16, textAlign: 'center'}}>Insufficient funds in your wallet.</Text> */}
                                <Text style={{fontSize: 18, textAlign: 'center'}}>Your tour enquiry has been placed successfully.</Text>

                                <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                                
                                    <View style={{margin:10}}>
                                        <Button block success onPress={() => {
                                            this.setState({ modalVisible: false });
                                        }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                            <Text style={{ color: "#ffffff",fontSize:14 }}> Okay </Text>
                                        </Button>
                                    </View>
                                </View>

                            </Form>
                        </View>
                    </View>
                </Modal>  

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.verifyModalVisible}
                    onRequestClose={() => {
                        this.setState({
                            verifyModalVisible: false
                        })
                    }}>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                        <View style={{ padding: 30, backgroundColor: '#fff' }}>
                            <Form>
                                {/* <Text style={{color: 'red', fontSize: 16, textAlign: 'center'}}>Insufficient funds in your wallet.</Text> */}
                                <Text style={{fontSize: 18, textAlign: 'center'}}>You have to verify your email before purchase can be made.</Text>

                                <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                                
                                    <View style={{margin:10}}>
                                        <Button block success onPress={() => {
                                            this.setState({ verifyModalVisible: false });
                                        }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                            <Text style={{ color: "#ffffff",fontSize:14 }}> OK </Text>
                                        </Button>
                                    </View>

                                    <View style={{margin:10}}>
                                        { this.state.verifyLoading ? <Spinner color="#0f7482" /> :
                                        <Button block success onPress={() => {
                                            this._sendVerification()
                                        }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                            <Text style={{ color: "#ffffff", fontSize:14 }}> Resend Verification </Text>
                                        </Button>
                                        }
                                    </View>
                                </View>

                            </Form>
                        </View>
                    </View>
                </Modal>

            </Content>
        </Container>
    )
  }

    onChanged(text){
        let newText = '';
        let numbers = '0123456789';

        for (var i=0; i < text.length; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            }
            else {
                // your call back function
            }
        }
        
        this.setState({ no_travellers: newText });
    }

    onChangedPhone(text){
        let newText = '';
        let numbers = '+0123456789 -';
    
        for (var i=0; i < 15; i++) {
            if(numbers.indexOf(text[i]) > -1 ) {
                newText = newText + text[i];
            }
            else {
                // your call back function
            }
        }
        
        this.setState({ phone: newText });
    }

    _sendVerification = () => {
        this.setState({
            verifyLoading: true
        })
        fetch( global.BASEURL + '/wp-json/auth/verifyAccount', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'authorization':this.state.access_token
            },
        })
        .then((response) => response.json())
        .then((response) => {            
            setTimeout(function(){
                if(response.status == 200){
                    ToastAndroid.show("Verification link has been sent to your register E-mail ID.",Toast.SHORT,Toast.TOP,SUCCESS_TOAST_STYLE); 
                } else {
                    ToastAndroid.show(response.message,Toast.SHORT,Toast.TOP,FAILURE_TOAST_STYLE); 
                }
            },600)
            this.setState({
                verifyModalVisible: false,
                verifyLoading: false
            });
            
            
        }).catch(error => {
            
        })
    }

    bookTour = () => {
        this.setState({
            isLoading: true
        });
        
        const data = {
            trip_date : this.state.date,
            no_travellers : this.state.no_travellers,
            tour_id:this.props.tour_id,
            phone: this.state.phone,
            country_code: this.state.selectedService
        }
        
        const json = JSON.stringify(data);
        
        if(this.state.access_token == null) {
            
            ToastAndroid.show("Please Login to continue.",Toast.SHORT,Toast.TOP,FAILURE_TOAST_STYLE); 
            
            const rowData = data;
            global.bookTour = true;
            this.props.navigation.navigate('Account',{rowData});
            // this.props.navigation.navigate('TourLogin');  
            
        } else {   

            let errorMsg = '';
            let errorFlag = false;
            
                if(data.trip_date == ''){
                    errorFlag = true;
                    errorMsg = 'Please select date';
                }

                if(data.phone == ''){
                    errorFlag = true;
                    errorMsg = 'Please enter phone';
                }

                if(data.country_code == ''){
                    errorFlag = true;
                    errorMsg = 'Please select country code';
                }
                
                if(data.no_travellers == null || data.no_travellers == '' ){
                    errorFlag = true;
                    errorMsg = 'Please enter No. of Travellers';
                }
            
                if(data.no_travellers) {
                    if(data.no_travellers.length > 3) {
                        errorFlag = true;
                        errorMsg = 'Maximum No. of Travellers is 999';
                    }
                }
            
                if(data.no_travellers == 0 || data.no_travellers == '0') {
                    errorFlag = true;
                    errorMsg = 'Please enter a valid number';
                }

                if(errorFlag) {
                    this.setState({
                        isLoading: false
                    });
                    ToastAndroid.show(errorMsg,Toast.SHORT,Toast.TOP,FAILURE_TOAST_STYLE); 
                    return true;
                }

                fetch(AUTHURL + 'tourEnquiry', {
                    method: 'POST',
                    headers: {
                    'Content-Type': 'application/json',
                    'authorization': this.state.access_token
                    },
                    body: json
                })
                .then((response) => response.json())
                .then((response) => {
                    
                    if(response.status == 401) {
                        this.setState({
                            verifyModalVisible : true,
                            isLoading: false
                        })

                    } else {
                        if(response.status == 200){
                            this.setState({
                                isLoading: false,
                                data:null,
                                no_travellers:null,
                                modalVisible: true
                            });
                            
                        } else {
                            this.setState({
                            isLoading: false
                            });
                            
                            ToastAndroid.show(response.message,Toast.SHORT,Toast.TOP,FAILURE_TOAST_STYLE); 
                        
                        }
                    }
                }) 
            }
        }
    }

const styles = {

}

export default Tourdetails;