import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet } from 'react-native'
import { Container, Content, Tab, Tabs, TabHeading, Spinner } from 'native-base';
import {CachedImage} from "react-native-img-cache";

class Tourimage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      imageLoading: false,
    }
  }

  _onLoad = () => {
    this.setState(() => ({ imageLoading: true }))
  }

  _onLoadEnd = () => {
    this.setState(() => ({ imageLoading: false }))
  }

  render () {
    return (
        <View style={{height : 160}}>
            <CachedImage style={{flex : 1}} resizeMode="cover" onLoadStart={() => this._onLoad()}
                      onLoadEnd={() => this._onLoadEnd()} source={{ uri : this.props.thumbnail }} />
                      {
                        this.state.imageLoading &&
                        <View style={styles.loadingView}>
                          <Spinner color="#ffffff" />
                        </View>
                      }
        </View>
    )
  }
}


const styles = {
  loadingView: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,.5)'
  },
  loadingImage: {
    width: 60,
    height: 60,
  }
}

export default Tourimage;