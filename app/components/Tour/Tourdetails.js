import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet } from 'react-native'
import { Container, Content, Card, CardItem, Left, Thumbnail, Body, Icon} from 'native-base';

class Tourdetails extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    return (
        <Container>
            <Content style={{padding: 10, backgroundColor: '#f0f0f0'}}>
                <Card style={{flexWrap:'nowrap'}} >
                    <CardItem >
                        <Body>
                            <Text style={styles.dealTitle}>{this.props.post_title}</Text>
                            <Text style={styles.dealPrice}>From : N{this.props.tour_price}</Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card style={{flexWrap:'nowrap'}} >
                    <CardItem>
                        <Body>
                            <Text style={styles.dealDesc}>
                                {this.props.post_content}
                            </Text>
                        </Body>
                    </CardItem>
                </Card>
            </Content>
        </Container>
    )
  }
}

const styles = {
  dealTitle: {
      color: '#0f7482',
      fontSize:16,
  }, 
  dealPrice:{
    color: '#575757',
    fontSize:16,
  },
  dealDesc:{
    color: '#575757',
    fontSize:14,
  }, 
  icon : {
      color: '#0f7482',
  }, 
  dealAboutCompany : {
    color: '#575757', 
    fontSize: 14, 
    paddingLeft: 10,
    paddingRight: 10, 
    paddingTop: 20,
    paddingBottom: 20
  }
}

export default Tourdetails;