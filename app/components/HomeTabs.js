import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet } from 'react-native'
import { Container, Content, Tab, Tabs, TabHeading } from 'native-base';

import HomeTabList from './HomeTabList.js';

class HomeTabs extends Component {

  constructor(props) {
    super(props);
  }

  render () { 
    return (
      <Tabs tabBarPosition = 'top' tabBarUnderlineStyle={{backgroundColor:'#ffffff'}} style={styles.tab}>
        <Tab heading={ <TabHeading style={styles.TabHeading} ><Text style={styles.TabHeadingText}>Hot Deals</Text></TabHeading>}>
          <HomeTabList params={{}} hotdeals='y' sortBy='date' needAuth={false} navigation={this.props.navigation}  />
        </Tab>
        <Tab heading={ <TabHeading style={styles.TabHeading}><Text style={styles.TabHeadingText}>Nearby</Text></TabHeading>}>
          <HomeTabList params={{}} hotdeals='n' sortBy='dist' needAuth={false} navigation={this.props.navigation} />
        </Tab>
        <Tab heading={ <TabHeading style={styles.TabHeading}><Text style={styles.TabHeadingText}>Favourites</Text></TabHeading>}>
          <HomeTabList params={{}} hotdeals='n' sortBy='fav' needAuth={true} navigation={this.props.navigation} />
        </Tab>
        <Tab heading={ <TabHeading style={styles.TabHeadingFilter}><Text style={styles.TabHeadingFilterText}>A-Z</Text></TabHeading>}>
          <HomeTabList params={{}} hotdeals='n' sortBy='name' needAuth={false} navigation={this.props.navigation} />
        </Tab>
      </Tabs>
    )
  }
}


const styles = {
    TabHeadingText:{
        color: '#ffffff',
    },
    TabHeadingFilterText:{
        color: '#ffffff',
    },
    TabHeading:{
        backgroundColor: '#0f7482',
    },
    TabHeadingFilter:{
        backgroundColor: '#f5ad2a'
    }
}

export default HomeTabs;