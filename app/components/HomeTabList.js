import React, { Component } from 'react'
import { Text, View, Image, Dimensions, BackHandler, ListView, StyleSheet, NetInfo, RefreshControl, AsyncStorage, ToastAndroid } from 'react-native';
import { Container, Content, List, ListItem, Left, Header, Item, Icon, Input, Button, Thumbnail, Body, Card, CardItem, Right, Spinner } from 'native-base';
import {CachedImage} from "react-native-img-cache";
import Toast from 'react-native-toast-native';

import DealsAPI from '../utils/Deals';
import renderIf from '../functions/renderif';

class HomeTabList extends Component {

  constructor(props) {
    super(props);
    this.navigateDealDetails = this.navigateDealDetails.bind(this);
    this._clearSearch = this._clearSearch.bind(this);
    this.state = {
        isLoading: true,
        isLocationReady: false,
        refreshing: false,
        dataSource: null,
        isConnected: 'offline',
        page:0,
        _data: null, 
        isLoadingMore: false,
        access_token: null, 
        showSearch: false,
        searchTerm: '',
        noDeals: false
    }
  }

    _fetchData(callback) {
        var data = {};
       
        navigator.geolocation.getCurrentPosition((position) => {
            console.log('HERE INSIDE IF> LOCATION');
            var data ={
                lat : position.coords.latitude,
                lng : position.coords.longitude,
                sortby : this.props.sortBy,
                hotdeals: this.props.hotdeals, 
                page: this.state.page,
                search: this.state.searchTerm
            }
            if(this.props.params && this.props.params.cat_id != null){
                data.cat_id = this.props.params.cat_id;
            }
            DealsAPI.getDealsList(callback,data,this.state.access_token); 
        }, (err) => {
            console.log('HERE INSIDE ELSE> LOCATION');
            var data ={
                sortby : this.props.sortBy,
                hotdeals: this.props.hotdeals, 
                page: this.state.page,
                search: this.state.searchTerm
            }
            if(this.props.params && this.props.params.cat_id != null){
                data.cat_id = this.props.params.cat_id;
            }
            DealsAPI.getDealsList(callback,data,this.state.access_token); 
        }, { enableHighAccuracy: true,  timeout: 1000 }) 
    
    }

  _fetchMore() {
    if(this.state.count == 10){
        this.setState({ page: this.state.page + 1});
        this.setState({ isLoadingMore: true});
        this._fetchData(responseJson => {
            if(responseJson.status == 200){
                const data = this.state._data.concat(responseJson.data);
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(data),
                    isLoadingMore: false,
                    _data: data,
                    count: responseJson.count
                });
            }else{
                this.setState({ isLoadingMore: false });
                this.setState({ page: this.state.page - 1});
            }
        });
    }
  }

  onRegionChange(region, lastLat, lastLong) {
    this.setState({
      isLoadingMore: true,
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
  }

  componentDidMount() {
    
    if(this.props.showSearch){
        this.setState({
            showSearch: true
        });
    }
    this._fetchData(responseJson => {
        
        if(responseJson.status == 404) {
            this.setState({
                isLoading: false,
                noDeals: true,
            });
        } else {
            
            let ds = new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2,
            });
            const data = responseJson.data;
           
            this.setState({
                dataSource: ds.cloneWithRows(data),
                isLoading: false,
                _data:data,
                count: responseJson.count,
            });
        }
    });
    
    AsyncStorage.getItem('user_token', (err, result) => {
        this.setState({access_token: result });
    });

    BackHandler.addEventListener('hardwareBackPress', function() {
        // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
        // Typically you would use the navigator here to go to the last state.
        global.currentRoute = 'Home';
    });
  }

  _onRefresh() {
  }

  _onEndReached() {
    this._fetchMore();
  }

  _clearSearch(){
    this.setState({
        searchTerm: '',
        page: 0,
        isLoading: true
    });
    
    this._fetchData(responseJson => {
        let ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
        });
        const data = responseJson.data;
        this.setState({
            dataSource: ds.cloneWithRows(data),
            isLoading: false,
            _data:data,
            count: responseJson.count
        });
    });
  }

  navigateDealDetails(rowData) {
    if(this.props.controlPanel) {
        this.props.navigation.navigate('Dealdetails',{rowData});
    } else {
        global.navigateBack = true;
        
        this.props.navigation.navigate('Dealdetails',{rowData});
    }
    
  }

  filterDeals() {
      this.setState({
         isLoading: true,
         page:0 
      });
    this._fetchData(responseJson => {
        if(responseJson.status == 200){
            let ds = new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2,
            });
            const data = responseJson.data;
            this.setState({
                dataSource: ds.cloneWithRows(data),
                isLoading: false,
                _data:data,
                count: responseJson.count
            });
        } else {
            
            ToastAndroid.show('No Deals found. Please try again',Toast.SHORT); 
            
            this.setState({
                isLoading: false,
            });
        }
    });
  }

  render () {
      const showKeyboard = this.props.showSearch;
    if (this.state.isLoading) {
        return (
            <Container>
                <Content>
                    <Spinner color="#0f7482" />
                </Content>
            </Container>
        );
    }

    if (this.state.noDeals) {
        if ( this.props.sortBy == 'fav' ) {
            return (
                <Container>
                    <Content>
                        <View style={{ alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color: '#0f7482', textAlign:'center',paddingTop: 10, fontSize:16 }}>You’ve not added any deal to favourites.</Text>
                        </View>
                    </Content>
                </Container>
            );
        } else {
            return (
                <Container>
                    <Content>
                        <View style={{ alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color: '#0f7482', textAlign:'center',paddingTop: 10, fontSize:16 }}>No Deals Found.</Text>
                        </View>
                    </Content>
                </Container>
            );    
        }
    }

    if( this.props.needAuth && this.state.access_token == null ){
        this.props.navigation.navigate('Account');
    }

    return (
    <Container style={{ backgroundColor: '#f0f0f0', paddingTop: 0, paddingBottom:0}}>

        { this.state.showSearch ?
            <Header searchBar rounded style={{backgroundColor: '#0f7482', paddingBottom:7 }}  androidStatusBarColor="#0b5864" iosBarStyle='light-content' noShadow={this.props.noShadow} backgroundColor="#0f7482"  >
                <Item>
                    <Icon name="ios-search" />
                    <Input placeholder="Search" value={this.state.searchTerm} onChangeText={(text) => {
                        this.setState({
                            searchTerm: text
                        });
                    }} onSubmitEditing={(text) => {
                        this.filterDeals();
                    }}  />
                    {this.state.searchTerm ? 
                        <Icon button onPress={this._clearSearch} name="ios-close" />: null
                    }
                </Item>
            </Header>
            : null
        } 
        
        <ListView
            legacyImplementation={true}
            style={styles.listItemBody}
            initialListSize={10}
            onEndReachedThreshold={100}
            pageSize={10}
            enableEmptySections={ true }
            automaticallyAdjustContentInsets={ false }
            dataSource={this.state.dataSource}

            renderRow={(rowData) =>
                <ListItem style={styles.listItem}>
                    <Card>
                        <CardItem style={{padding: 0, margin: 0}} button onPress={() => this.navigateDealDetails(rowData)}>
                            <Left>
                                <Thumbnail square source={{uri: rowData.thumb }} />
                                <Body>
                                    <Text style={styles.dealTitle}>{rowData.post_title}</Text>
                                    <Text style={styles.dealDesc}>{rowData.description}</Text>
                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                </ListItem>
            }
            onEndReached={ () => this._onEndReached() }
            renderFooter={() => {
                return (
                this.state.isLoadingMore &&
                <View style={{ flex: 1, padding: 10 }}>
                    <Spinner color="#0f7482" />
                </View>
                );
            }}
        />
        
        {renderIf(this.isLoadingMore, 
            <View>
                <Spinner color="#0f7482" />
            </View>
        )}

    </Container>
    )
  }
}


const styles = {
    listItemBody:{
        backgroundColor: "#f0f0f0", 
        paddingTop: 10,
        padding: 0, 
        margin:0, 
        borderWidth: 0
    },
    listItem: {
        backgroundColor: "#f0f0f0", 
        paddingBottom: 0, 
        marginBottom:0, 
        paddingTop: 0, 
        marginTop:0, 
        borderWidth:0,
        borderBottomWidth:0,
    },
    dealTitle:{
        color: '#0f7482',
        fontSize:16
    },
    dealDesc:{
        color: '#575757',
        fontSize:14
    },
    TabHeadingText:{
        color: '#ffffff',
    },
    TabHeadingFilterText:{
        color: '#ffffff',
    },
    TabHeading:{
        backgroundColor: '#0f7482',
    },
    TabHeadingFilter:{
        backgroundColor: '#f5ad2a'
    }
}

export default HomeTabList;