import React, { Component } from 'react';
import { Text, View, Image, Dimensions, StyleSheet, ScrollView, AsyncStorage, Spinner, Modal } from 'react-native'
import { Container, Content, Button, Card, CardItem, TabHeading, Body, List, ListItem, Left, Toast, Icon } from 'native-base'

import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';


const logo = require('./img/logo.png')
const dealIcon = require('./img/menuassets/discount-voucher-3x.png')

const eventIcon = require('./img/menuassets/event-date-and-time-symbol-3x.png')
const galleryIcon = require('./img/menuassets/gallery-3x.png')
const aboutIcon = require('./img/menuassets/icon-3x.png')
const tourIcon = require('./img/menuassets/mask-group-3x.png')
const cartIcon = require('./img/menuassets/shopping-cart-3x.png')
const starIcon = require('./img/menuassets/star-3x.png')
const userIcon = require('./img/menuassets/user-3x.png')
const homeIcon = require('./img/menuassets/home.png')
const flightIcon = require('./img/menuassets/departures.png')

function getListClassname(routeName) {
    if(routeName == this.state.routeName) {
        return 'listItem';
    } else {
        return 'listItemActive';
    }
}

class ControlPanel extends Component {

    
    constructor(props) {
        super(props);
        this.logoutUser = this.logoutUser.bind(this);
        this.state = {
            access_token: null,
            routeName: '',
            isLoading: true,
            showModel: false,
            lat: null,
            lng: null,
            notificationList: [],
        };

    }    

    openthePopup(){
        this.setState({
            showModel: true
        }); 
    }

    logoutUser() {
        AsyncStorage.removeItem('user_token');
        AsyncStorage.removeItem('user_name');
        AsyncStorage.removeItem('userData');
        this.setState({access_token: null });
        global.access_token = null;
        global.user_name = '';

        this.props.navigation.navigate('Home');
        
    }

    componentDidMount(){
        
       
        AsyncStorage.getItem('userData', (err, result) => {
            if(result) {
                let user_data = [];

                user_data = JSON.parse(result);
                // alert(user_data.token);
                this.setState({ 
                    access_token: user_data.token
                });  
            }
        });    
        
        if( global.currentRoute == '' ||  global.currentRoute == undefined ) {
            global.currentRoute = 'Home';
        }
        
        navigator.geolocation.getCurrentPosition((position) => {
            
            this.setState({
                lat : position.coords.latitude,
                lng : position.coords.longitude,
            });
          
        }, (err) => {
           console.log(err.message);
       }, { enableHighAccuracy: true, timeout: 1000}) 

        this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
            
            console.log('I M HERE');

            // if(notif.ID) {
            //     this.props.navigation.navigate('Dealdetails',{rowData: notif});
            // }
           
            if(notif.opened_from_tray) {
                
                if ( notif.backend == 'Y') {
                    
                    if ( notif.redeemed == 'Y') {
                        AsyncStorage.getItem('notifDeal',(err,result) => {
                            
                            if(result) {
                                let data = JSON.parse(result);
                                this.setState({ notificationList: data });    
                            }
                            
                            this.state.notificationList.push(notif);
                            let newList = this.state.notificationList;
                            
                            AsyncStorage.setItem('notifDeal', JSON.stringify(newList));
                            this.props.navigation.navigate('NotificationList');
        
                        });
                    } else {

                        AsyncStorage.getItem('notifDeal',(err,result) => {
                            
                            if(result) {
                                let data = JSON.parse(result);
                                this.setState({ notificationList: data });    
                            }
                            
                            this.state.notificationList.push(notif);
                            let newList = this.state.notificationList;
                            
                            AsyncStorage.setItem('notifDeal', JSON.stringify(newList));        
                        });

                        if(notif.type == 'Deals') {
                            this.props.navigation.navigate('Dealdetails',{rowData: notif});
                        } else {
                            this.props.navigation.navigate('Newsdetail',{rowData: notif});
                        }
                        
                    }
                } else {
                    var data = {
                        lat : this.state.lat,
                        lng : this.state.lng,
                    }
                    const json = JSON.stringify(data);
                   
                    fetch( APIURL + 'getNearestDeals', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: json
                    })
                    .then((response) => response.json())
                    .then((response) => {
                    
                        if(response.status == 200){
                            
                            AsyncStorage.getItem('notifDeal',(err,result) => {
                                let data = JSON.parse(result);
                                this.setState({ notificationList: data });
                            });
                            
                            this.state.notificationList.push(response.data);
                            let newList = this.state.notificationList;
                            
                            AsyncStorage.setItem('notifDeal', JSON.stringify(newList));
                            
                            this.props.navigation.navigate('Dealdetails',{rowData: response.data});
                        
                        }
                    });

                    if(notif.data.title == "Wallet" || notif.data.title == "Order History") {
                        AsyncStorage.getItem('notifDeal',(err,result) => {
                            
                            if(result) {
                                let data = JSON.parse(result);
                                this.setState({ notificationList: data });    
                            }
                            
                            this.state.notificationList.push(notif.data);
                            let newList = this.state.notificationList;
                            
                            AsyncStorage.setItem('notifDeal', JSON.stringify(newList));
        
                            this.props.navigation.navigate('NotificationList');
                        });
                    } 
                }
            }
               
        });
     
    }

    manageRoute(route) {
        global.currentRoute = route;
    }
 
    render() {
        const { navigate } = this.props.navigation;
        var searchNavprops = {};
        searchNavprops.openSearch = true;
        
        return (
            <Container style={styles.controlContainer}>
            <Content>
                <View style={styles.controlHeader}>
                    <View style={styles.logoContainer}>
                        <Image style={styles.logo} onPress={() => navigate('Home') } source={logo} />
                    </View>

                { this.state.access_token != null || global.access_token != null ?
                    <View style={{flex:1, flexDirection:'row'}}>
                        <View style={{margin:10}}>
                            <Button block success onPress={() => navigate('ManageAccount',this.manageRoute( 'ManageAccount' ))} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                <Text style={{ color: "#ffffff",fontSize:18 }}> Account </Text>
                            </Button>
                        </View>

                        <View style={{margin:10}}>
                            <Button block danger onPress={this.logoutUser} style={{ padding:10 }}>
                                <Text style={{ color: "#ffffff",fontSize:18 }}> Logout </Text>
                            </Button>
                        </View>
                    </View>
                    :
                    <Text style={{color: '#0f7482', paddingTop:10, fontSize:18}} onPress={() => navigate('Account') }>Login or Signup for Free</Text>
                }
                    
                </View>
                <View style={styles.controlContent}>
                    <List itemDivider={false} style={{backgroundColor: '#0f7482', marginTop: 14}}>

                        { global.currentRoute == "Home"  ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('Home',this.manageRoute( 'Home' )) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={homeIcon} />
                                <Body>
                                    <Text style={styles.listItemText}>Home</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('Home',this.manageRoute( 'Home' )) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={homeIcon} />
                                <Body>
                                    <Text style={styles.listItemText}>Home</Text>
                                </Body>
                            </Left>
                        </ListItem> }

                        { global.currentRoute == "Deals" ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('Deallist',this.manageRoute('Deals'))  } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={dealIcon} />
                                <Body>
                                    <Text style={styles.listItemText}>Deals & Offers</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('Deallist',this.manageRoute('Deals')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={dealIcon} />
                                <Body>
                                    <Text style={styles.listItemText}>Deals & Offers</Text>
                                </Body>
                            </Left>
                        </ListItem> }

                        { global.currentRoute == "Lagostour" ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('Tourlist',this.manageRoute('Lagostour')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={tourIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Lagos tours</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('Tourlist',this.manageRoute( 'Lagostour' )) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={tourIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Lagos tours</Text>
                                </Body>
                            </Left>
                        </ListItem> }

                        { global.currentRoute == "LagosInfo" ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('LagosInfo',this.manageRoute( 'LagosInfo' )) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={starIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Lagos Info</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('LagosInfo',this.manageRoute( 'LagosInfo' )) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={starIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Lagos Info</Text>
                                </Body>
                            </Left>
                        </ListItem> }


                        { global.currentRoute == "Gallery" ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('Gallery',this.manageRoute('Gallery')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={galleryIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Gallery</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('Gallery',this.manageRoute('Gallery')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={galleryIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Gallery</Text>
                                </Body>
                            </Left>
                        </ListItem> }


                        { global.currentRoute == "Events" ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('Events',this.manageRoute('Events')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={eventIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Events</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('Events',this.manageRoute('Events')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={eventIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Events</Text>
                                </Body>
                            </Left>
                        </ListItem> }
                        
                        { global.currentRoute == "Shop" ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('Shop',this.manageRoute('Shop')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={cartIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Hotel Booking</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('Shop',this.manageRoute('Shop')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={cartIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Hotel Booking</Text>
                                </Body>
                            </Left>
                        </ListItem> }

                        { global.currentRoute == "FlightBooking" ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('FlightBooking',this.manageRoute('FlightBooking')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={flightIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Flight Booking</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('FlightBooking',this.manageRoute('FlightBooking')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={flightIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>Flight Booking</Text>
                                </Body>
                            </Left>
                        </ListItem> }

                        { global.routeName == "About" ? 
                        <ListItem button itemDivider={false} style={styles.listItemActive} onPress={() => navigate('About',this.manageRoute('About')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={aboutIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>About</Text>
                                </Body>
                            </Left>
                        </ListItem> : 
                        <ListItem button itemDivider={false} style={styles.listItem} onPress={() => navigate('About',this.manageRoute('About')) } >
                            <Left>
                                <Image style={styles.menuItemIcon} source={aboutIcon} /> 
                                <Body>
                                    <Text style={styles.listItemText}>About</Text>
                                </Body>
                            </Left>
                        </ListItem> }
                    </List>
                </View>
                
                <Modal
                    transparent={true}
                    style={styles.model}
                    onRequestClose= {() => {
                        this.setState({
                            showModel: false
                        });
                    }}
                    visible={this.state.showModel}>
                    <View style={{backgroundColor:'rgba(0,0,0,0.5)', flex:1}}></View>
                    <View style={styles.modelin}>
                        <Text style={styles.modelText}>This feature will be available in the next version. Thank you!</Text>
                    </View>
                </Modal>
            </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    controlContainer: {
        backgroundColor: '#0f7482',
    },
    listItem:{
        paddingLeft:17,
        marginLeft:0,
        backgroundColor: '#0f7482',
        borderBottomWidth: 0,
    },
    listItemActive:{
        paddingLeft:17,
        marginLeft:0,
        backgroundColor: '#0b5864',
        borderBottomWidth: 0
    },
    listItemText:{
        color: '#ffffff',
        fontSize:18
    }, 
    logo: {
        marginTop:10,
        width: 134,
        height: 70,
    },
    menuItemIcon:{
        width:24,
        height:23,
    },
    logoContainer:{
        flex:2,
        flexDirection: 'column',
        justifyContent: 'center', 
        alignItems: 'center'
    },
    userText:{
        fontSize: 16,
        padding: 5,
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    controlHeader: {
        flex:1,
        backgroundColor: '#ffffff',
        padding: 20,
        flexDirection: 'column',
        justifyContent: 'center', 
        alignItems: 'center'
    },
    controlContent:{
        flex:3,
    }, 
    model:{
        backgroundColor:'rgba(0,0,0,0.5)',
        flex: 1,
        marginTop: 100,
        marginLeft:20,
        marginRight:20,
        height: 200,
        flexDirection:'row'
    },
    modelin: {
        position: 'absolute', 
        left: 0, 
        right: 0, 
        bottom: 0,
        top: 0,
        height: 120,
        backgroundColor:"#ffffff",
        justifyContent:"center",
        marginTop: 200,
        marginLeft:20,
        marginRight:20,
        padding: 20,
    }, 
    modelText:{
        fontSize: 16,
        fontWeight: 'bold'
    }
});

export default ControlPanel;