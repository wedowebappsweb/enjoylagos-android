import React, { Component } from 'react'
import { Text, View, Image, Dimensions,TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { Spinner } from 'native-base';
import {CachedImage} from "react-native-img-cache";
import Swiper from 'react-native-swiper'

const { width } = Dimensions.get('window')
const loading = require('./img/loading.gif')

import BannersAPI from '../utils/Banners';

const styles = {
  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  image: {
    width,
    flex: 1,
    backgroundColor: 'transparent'
  },
  bannerHeading: {
    alignItems: 'center',
    backgroundColor: '#0b5864',
    alignItems: 'center',
    color:'#ffffff',
    padding: 10,
    fontSize: 16,
    textAlign: 'center',
  },
  loadingView: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,.5)'
  },
  loadingImage: {
    width: 60,
    height: 60,
  }
}

const Slide = props => {
  return (
    <View style={styles.slide} >
      <TouchableWithoutFeedback onPress={() => props.navigateOffers(props.linkTo,props.rowData)}>
        <CachedImage resizeMode="cover" title={<Text style={styles.bannerHeading} numberOfLines={1}>Aussie tourist dies at Bali hotel</Text>} onLoad={props.loadHandle.bind(null, props.i)} style={styles.image} source={{uri: props.uri}} />
      </TouchableWithoutFeedback>
          {
          !props.loaded && <View style={styles.loadingView}>
              <Image height={100} style={styles.loadingImage} source={loading} />
              
          </View>
          } 
      <Text style={styles.bannerHeading}>{props.heading}</Text>
    
    </View>
  )
}

export default class extends Component {
    
  constructor (props) {
    super(props)
    this.state = {
      imgList: [],
      loadQueue: [],
      isLoading: true
    }
    this.loadHandle = this.loadHandle.bind(this);
    this.navigateOffers = this.navigateOffers.bind(this);
  }
  
  loadHandle (i) {
    let loadQueue = this.state.loadQueue
    loadQueue[i] = 1 
    this.setState({
      loadQueue
    })
  }

  navigateOffers(linkTo,rowData) {
      
      if(rowData){
        global.navigateBack = true;
        if(linkTo == 'deal') {
          this.props.navigation.navigate('Dealdetails',{rowData});
        } else {
          this.props.navigation.navigate('Tourdetail',{rowData});
        }
      }

  }

  _fetchData(callback) {
    BannersAPI.getDealsList(callback); 
  }

  componentWillMount(){
    
    this._fetchData(responseJson => {
        const data = responseJson.data;
        this.setState({
          imgList : data,
          isLoading: false
        });
    });
  }

  render () {
    if (this.state.isLoading) {
      return (
        <View style={{height:210}}>
          <View style={styles.loadingView}>
            <Image height={100} style={styles.loadingImage} source={loading} />
          </View>
        </View>
      );
    }
    
    return (
      <View style={{height:210}}>
        <Swiper 
            loadMinimal 
            loadMinimalSize={1} 
            showsPagination={false} 
            
            style={styles.wrapper} 
            loop={false}
        >
        {
            this.state.imgList.map((item, i) => <Slide
                loadHandle = {this.loadHandle}
                navigateOffers = {this.navigateOffers}
                loaded = {!!this.state.loadQueue[i]}
                uri = {item.thumbnail}
                heading = {item.post_title}
                rowData = {item.rowData}
                linkTo = {item.link_to}
                postId = {item.post_id}
                i = {i}
                key = {i} 
            />)
        }
        </Swiper>
        
      </View>
    )
  }
}