import React, { Component } from 'react'
import { AppRegistry, TouchableOpacity, Text, View, Image, Dimensions, StyleSheet } from 'react-native'
import { Container, Content, Card, CardItem, Left, Thumbnail, Body, Icon} from 'native-base';

import Communications from 'react-native-communications';
import openMap from 'react-native-open-maps';

class Dealitems extends Component {

    constructor(props) {
        super(props);
    }

    _openMap(lat,lng) {
        if(typeof lat != 'undefined' && typeof lng != 'undefined'){
            openMap({ latitude: lat, longitude: lng });
        }
    }

  render () {
    return (
        <Container>
            <Content style={{padding: 10, backgroundColor: '#f0f0f0'}}>
                <Text style={styles.dealAboutCompany}>{this.props.description}</Text>
                <Card style={{flexWrap:'nowrap'}}>
                <TouchableOpacity onPress={() => this._openMap(this.props.lat,this.props.lng)}>
                    <CardItem style={{padding: 0, margin: 0}} >
                        <Left>
                            <Icon style={styles.renderIcon} name='ios-home' />
                            <Body>
                                <Text style={styles.dealTitle}>{this.props.address}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                    </TouchableOpacity>
                </Card>
                <Card style={{flexWrap:'nowrap'}}>
                    <CardItem style={{padding: 0, margin: 0}}>
                        <Left>
                          <Icon style={styles.renderIcon} name='ios-call' />
                            <Body>
                                <TouchableOpacity onPress={() => Communications.phonecall(this.props.phone, true)}>
                                    <Text style={styles.dealTitle}>{this.props.phone}</Text>
                                </TouchableOpacity>    
                            </Body>
                        </Left>
                    </CardItem>
                </Card>
                <Card style={{flexWrap:'nowrap'}}>
                    <CardItem style={{padding: 0, margin: 0}}>
                        <Left>
                          <Icon style={styles.renderIcon} name='ios-mail' />
                            <Body>
                                <TouchableOpacity onPress={() => Communications.email([this.props.email], null, null, null, null)}>
                                    <Text style={styles.dealTitle}>{this.props.email}</Text>
                                </TouchableOpacity>  
                            </Body>
                        </Left>
                    </CardItem>
                </Card>
                
            </Content>
        </Container>
    )
  }
}

const styles = {
  dealTitle: {
      color: '#575757',
      fontSize:16,
      paddingLeft: 10
  }, 
  renderIcon : {
      color: '#0f7482',
  }, 
  dealAboutCompany : {
    color: '#575757', 
    fontSize: 14, 
    paddingLeft: 10,
    paddingRight: 10, 
    paddingTop: 20,
    paddingBottom: 20
  }
}

export default Dealitems;
AppRegistry.registerComponent('Dealitems', () => RNCommunications);
