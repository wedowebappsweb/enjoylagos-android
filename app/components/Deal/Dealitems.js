import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet, NetInfo, ListView, AsyncStorage, TouchableOpacity, ToastAndroid } from 'react-native'
import { Container, Content, Card, CardItem, Left, Right, Thumbnail, Body, Tex, Icon, ListItem, Spinner } from 'native-base';

import DealsAPI from '../../utils/Deals';
import Toast from 'react-native-toast-native';

class Dealitems extends Component {

  constructor(props) {
    super(props);
    this.state = {
        isLoading: true,
        dataSource: null,
        isConnected: 'offline',
        access_token: null, 
        item_id: null,
        deal_id: null,
        randomData: null,
        noData:false,
    }
  }

  _fetchData(callback) {
    deal_id = this.props.deal_id;
    DealsAPI.getDealItems(callback, deal_id);
  }

  componentDidMount(){  

    this._fetchData(responseJson => {
        console.log(responseJson);
        if(responseJson.status == 200){
            let ds = new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2,
            });
            const data = responseJson.data;
            
            this.setState({
                dataSource: ds.cloneWithRows(data),
                isLoading: false,
                randomData:data,
            });
        } else {
            this.setState({
                isLoading: false,
                noData:true
            });
        }
        
    });
    AsyncStorage.getItem('user_token', (err, result) => {
        this.setState({access_token: result });
    });
  }
  
  _likeItem(id, deal_id, callback) {
    DealsAPI.addToFav( id, deal_id, callback, this.state.access_token); 
  }

  likeDealItem(id, deal_id){
      if(this.state.access_token == null) {

        ToastAndroid.show("Please login to continue.",Toast.SHORT); 

        this.props.navigation.navigate('DealLogin');
      } else {
        this._likeItem(id, deal_id, responseJson => {
            
            let newArray = [];
            const newData = this.state.randomData;
            newData.forEach(element => {
            
                if(element.id == id) {
                    if(element.isLiked == "N"){
                        element.isLiked = "Y";
                    } else {
                        element.isLiked = "N";
                    }
                } else {

                }
                newArray.push(element);
            });
            let ds = new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2,
            });
            this.setState({
                dataSource: ds.cloneWithRows(newArray),
                
            });
        });

    }
  }

  venderDetails(rowData) {
      let deal_name = this.props.navigation.state.params.rowData.post_title;

      this.props.navigation.navigate('VendorDetails',{rowData,deal_name});
  }
  
  render () {

    if (this.state.isLoading) {
        return (
            <Container>
                <Content>
                    <Spinner color="#0f7482" />
                </Content>
            </Container>
        );
    }

    return (
        <Container>
            <Content style={{paddingTop: 10, backgroundColor: '#f0f0f0'}}>
             <View><Text style={{ textAlign:'center',paddingTop: 10, paddingBottom:10, paddingLeft:50,paddingRight:50, fontSize:12 }}> YOU NEED TO PAY USING A UBA CARD OR THE ENJOY LAGOS PREPAID CARD AT <Text style={{ fontWeight: 'bold'}}> {this.props.title} </Text> TO ENJOY THE DEAL(S) </Text></View>
                {
                    this.state.noData ? 
                    <Text style={{color: '#0f7482', textAlign:'center',paddingTop: 10, fontSize:16 }}>No Deals Found.</Text>: 
                    <ListView
                        legacyImplementation={true}
                        style={styles.listItemBody}
                        // onEndReached={ () => this._onEndReached() }
                        enableEmptySections={ true }
                        automaticallyAdjustContentInsets={ false }
                        dataSource={this.state.dataSource}
                        renderRow={(rowData) =>
                            <ListItem style={styles.listItem}>
                                <Card style={{flexWrap:'nowrap'}} >
                                    <CardItem style={{padding: 0, margin: 0}} onPress={this.navigateDealDetails}>
                                        <View style={{flexDirection: 'row', flex:1}}>
                                            <View style={{flex:1,justifyContent: 'flex-start', alignItems: 'center', alignSelf:'center'}}>
                                                <TouchableOpacity onPress={() => this.venderDetails(rowData)}>
                                                <Icon style={styles.renderIcon} name='ios-menu' />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{flex:10}}>
                                                <TouchableOpacity onPress={() => this.venderDetails(rowData)}>
                                                    <Text style={styles.dealTitle}>{rowData.content}</Text>
                                                    <Text style={styles.dealTitle}> ₦ {rowData.price}</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{flex:1, justifyContent: 'flex-end', alignItems: 'center', alignSelf:'center'}}>
                                                <TouchableOpacity onPress={() => this.likeDealItem(rowData.id, rowData.deal_id)}>
                                                {rowData.isLiked == 'N' ? 
                                                    <Icon style={styles.renderIcon} name='md-heart-outline' /> : 
                                                    <Icon style={styles.renderIcon} name='md-heart' />
                                                }
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </CardItem>
                                </Card>
                            </ListItem>
                        }
                    />
                }
                
                
                <View style={{height:20}}></View>
            </Content>
        </Container>
    )
  }
}


const styles = {
    listItemBody:{
        backgroundColor: "#f0f0f0", 
        padding: 0, 
        margin:0, 
        borderWidth: 0
    },
    listItem: {
        backgroundColor: "#f0f0f0", 
        paddingBottom: 0, 
        marginBottom:0, 
        paddingTop: 0, 
        marginTop:0, 
        borderWidth:0,
        borderBottomWidth:0,
    },
    dealTitle: {
        color: '#0f7482',
        fontSize:16,
        paddingLeft: 10
    }, 
    renderIcon : {
        color: '#0f7482',
    }
}

export default Dealitems;