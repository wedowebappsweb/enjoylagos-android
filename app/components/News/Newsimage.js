import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet } from 'react-native'
import { Container, Content, Tab, Tabs, TabHeading } from 'native-base';

class Newsimage extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    return (
        <View style={{height : 160}}>
            <Image style={{flex : 1}} resizeMode="cover" source={{ uri : this.props.thumbnail}} />
        </View>
    )
  }
}


const styles = {
}

export default Newsimage;