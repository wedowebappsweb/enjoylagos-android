import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet } from 'react-native'
import { Container, Content, Card, CardItem, TabHeading, Body } from 'native-base'
import HTMLView from 'react-native-htmlview';

class Newsdetails extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    return (
        <View style={{padding: 10}}>
            <Card style={{flexWrap:'nowrap'}} >
                <CardItem>
                    <Body>
                        <HTMLView
                            value={this.props.post_content}
                            stylesheet={styles}
                        />
                    </Body>
                </CardItem>
            </Card>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    a: {
        fontWeight: '300',
        color:'blue',
        borderBottomWidth:1
    }
});

export default Newsdetails;