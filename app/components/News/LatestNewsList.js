import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, NetInfo, ListView, ToastAndroid } from 'react-native'
import { Container, Content, List, ListItem, Left, Thumbnail, Body, Card, CardItem, Right, Spinner } from 'native-base';
import HTMLView from 'react-native-htmlview';
import NewsAPI from '../../utils/News';
import Toast from 'react-native-toast-native';


import {
    Analytics,
    Hits as GAHits,
    Experiment as GAExperiment
  } from 'react-native-google-analytics';
  import DeviceInfo from 'react-native-device-info';
  var ga = this.ga = null;
class LatestNewsList extends Component {
    constructor(props) {
        super(props);
        this.navigateNewsDetails = this.navigateNewsDetails.bind(this);
        this.state = {
            isLoading: true,
            dataSource: null,
            isConnected: 'offline',
            isLoadingMore: false,
            page: 0,
            _data: null
        }
    }

    _fetchData(callback) {
        var data ={
            page: this.state.page
        }
        NewsAPI.getNewsList(callback,data);
    }

    _onEndReached() {
        this._fetchMore();
    }

    _fetchMore() {
        this.setState({ page: this.state.page + 1});
        this._fetchData(responseJson => {
            if(responseJson.status == 200){
                const data = this.state._data.concat(responseJson.data);
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(data),
                    isLoadingMore: false,
                    _data: data,
                });
            }else{
                this.setState({isLoadingMore: false});
            }
        });
    }

    componentDidMount(){    

        let clientId = DeviceInfo.getUniqueID();
        
        ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
        
        var screenView = new GAHits.ScreenView(
          'EnjoyLagos App',
          'News Screen',
          DeviceInfo.getReadableVersion(),
          DeviceInfo.getBundleId()
        );
        ga.send(screenView);

        NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange);
        this._fetchData(responseJson => {
            let ds = new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2,
            });
            const data = responseJson.data;
            this.setState({
                dataSource: ds.cloneWithRows(data),
                isLoading: false,
                _data:data
            });
        });
    }

    _handleConnectionChange = (isConnected) => {
        
        if(isConnected == false) {

            ToastAndroid.show("Please check your internet connection.",Toast.SHORT); 
          
        }
      };
    
      componentWillMount(){
        NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange);
      }

    navigateNewsDetails(rowData) {
        this.props.navigation.navigate('Newsdetail', {rowData});
    }

    render () {
        var maxlimit = 100;
        if (this.state.isLoading) {
            return (
                <Container>
                    <Content>
                        <Spinner color="#0f7482" />
                    </Content>
                </Container>
            );
        }

        return (
            <View style={{flex:1}}> 
            <ListView
                legacyImplementation={true}
                style={styles.listItemBody}
                initialListSize={10}
                onEndReachedThreshold={100}
                pageSize={10}
                enableEmptySections={ true }
                automaticallyAdjustContentInsets={ false }
                dataSource={this.state.dataSource}
                scrollEnabled={true}
                renderRow={(rowData) =>
                    <ListItem style={styles.listItem}>
                        <Card>
                            <CardItem style={{padding: 0, margin: 0}} button onPress={() => this.navigateNewsDetails(rowData)}>
                                <Left>
                                    <Thumbnail square source={{ uri: rowData.thumb }} />
                                    <Body>
                                        <Text style={styles.dealTitle}>{rowData.post_title}</Text>
                                        <HTMLView
                                            value={ ((rowData.post_content).length > maxlimit) ? 
                                                (((rowData.post_content).substring(0,maxlimit-3)) + '...') : 
                                                rowData.post_content }
                                        />
                                    </Body>
                                </Left>
                            </CardItem>
                        </Card>
                    </ListItem>
                }
                onEndReached={ () => this.setState({ isLoadingMore: true }, () => this._onEndReached()) }
                renderFooter={() => {
                    return (
                    this.state.isLoadingMore &&
                    <View style={{ flex: 1, padding: 10 }}>
                        <Spinner color="#0f7482" />
                    </View>
                    );
                }}
            />
            </View>    
        )
    }
}


const styles = {
    listItemBody:{
        backgroundColor: "#f0f0f0", 
        padding: 0, 
        margin:0, 
        borderWidth: 0, 
        height: 560
    },
    listItem: {
        backgroundColor: "#f0f0f0", 
        paddingBottom: 0, 
        marginBottom:0, 
        paddingTop: 0, 
        marginTop:0, 
        borderWidth:0,
        borderBottomWidth:0,
    },
    dealTitle:{
        color: '#0f7482',
        fontSize:16
    },
    dealDesc:{
        color: '#575757',
        fontSize:14
    },
    TabHeadingText:{
        color: '#ffffff',
    },
    TabHeadingFilterText:{
        color: '#ffffff',
    },
    TabHeading:{
        backgroundColor: '#0f7482',
    },
    TabHeadingFilter:{
        backgroundColor: '#f5ad2a'
    }
}

export default LatestNewsList;