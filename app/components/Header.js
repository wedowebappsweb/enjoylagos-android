import React, { Component } from 'react';
import { StyleSheet, BackHandler, Image } from 'react-native';
import { Header, Title, Body, Button, Left, Right, Icon, Text, StyleProvider } from 'native-base';
import { NavigationActions } from 'react-navigation';
import renderif from '../functions/renderif';

function renderIf(condition, content) {
    if (condition) {
        return content;
    }
    return null;
}

class AppHeader extends Component {
    constructor(props) {
        super(props);
        this.navigateAccount = this.navigateAccount.bind(this);
        this.navigateBack = this.navigateBack.bind(this);
        this.navigateNotifications = this.navigateNotifications.bind(this);
        
        this.state = {
          showButtons : true,
          showSearch: true
        }

    }

    componentDidMount(){
        if(this.props.showButtons == false){
            this.setState({showButtons: false});
        }
        if(this.props.noSearch){
            this.setState({showSearch: false});
        }

    }

    navigateAccount() {
        this.props.navigation.navigate('User');
    }
    navigateNotifications() {
        this.props.navigation.navigate('NotificationList');
    }
    navigateBack() {
       
        if(this.props.manageAccount) {
            this.props.navigation.navigate('ManageAccount');
        } else {
            if(global.navigateBack == true && (this.props.navigation.state.routeName == "Dealdetails" || this.props.navigation.state.routeName == "Tourdetails")) {
                global.navigateBack =false;
                this.props.navigation.navigate('Home');
            }
            else {
                this.props.navigation.goBack();
            }    
        }
    }

    render() {
        var maxlimit = 30;
        var searchNavprops = {};
        searchNavprops.openSearch = true;   
        let routeName = this.props.navigation.state.routeName;
        return (
            
            <Header androidStatusBarColor="#0b5864" iosBarStyle='light-content' noShadow={this.props.noShadow} backgroundColor="#0f7482" style={styles.header} >
                <Left>
                    {renderIf(this.props.showBack, 
                        <Button transparent onPress={this.navigateBack}> 
                            <Icon name='arrow-back' style={styles.headericon} />
                        </Button>
                    )}
                    {renderIf(!this.props.showBack, 
                        <Button transparent onPress={() => {this.props.navigation.navigate('DrawerOpen') }}>
                            <Icon name='ios-menu' style={styles.headericon} />
                        </Button>
                    )}
                </Left>
                <Text style={{ alignSelf:'center' }}>
                <Title style={{color: "#ffffff"}}>{ ((this.props.title).length > maxlimit) ? 
                                            (((this.props.title).substring(0,maxlimit-3)) + '...') : 
                                            this.props.title }</Title>
                
                </Text>
                {renderIf(this.state.showButtons,
                <Right>
                    {/* {renderIf(this.state.showSearch , 
                        <Button transparent >
                            <Icon name='ios-search' style={styles.headericon} onPress={() => { this.props.navigation.navigate('Deallist',{searchNavprops}) } } />
                        </Button>
                    )} */}
                    { renderIf(routeName == 'Home',
                        <Button transparent onPress={this.navigateNotifications} >
                            <Icon name='ios-notifications' style={styles.headericon} />
                        </Button>)}
                    {/* {r      <Icon name='ios-notifications' style={styles.headericon} />
                enderIf(this.props.showAccount, 
                        <Button transparent onPress={this.navigateAccount} >
                        <Icon name='ios-contact' />
                        </Button>
                    )} */}
                </Right>
                )}
            </Header>
        );
    }
}

const styles = StyleSheet.create({
    header: {
      backgroundColor: '#0f7482',
    },
    headericon: {
        color: '#ffffff'
    }
});

export default AppHeader ;