import React, { Component } from 'react';
import { AsyncStorage, View, ListView, StyleSheet, TouchableHighlight, Animated } from 'react-native';
import { Container, Content, Text, ListItem, List, Card, CardItem, Left, Right, Thumbnail, Body, Spinner, Tab, Tabs, TabHeading } from 'native-base';

import MiscAPI from '../../utils/Misc';

export default class OrderHistory extends Component {
    
    anime = {
        height: new Animated.Value(),
        expanded: false,
        contentHeight: 0,
    }

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            refreshing: false,
            dataSource: null,
            page:0,
            _data: null, 
            isLoadingMore: false,
            access_token: null, 
            empty: false
        }

        this._initContentHeight = this._initContentHeight.bind(this);
        this.toggle = this.toggle.bind(this);

        this.anime.expanded = props.expanded;
    }
    
    _initContentHeight(evt) {
        if (this.anime.contentHeight>0) return;
        this.anime.contentHeight = evt.nativeEvent.layout.height;
        this.anime.height.setValue(this.anime.expanded ? this._getMaxValue() : this._getMinValue() );
    }

    _getMaxValue() { return this.anime.contentHeight };
    _getMinValue() { return 0 };

    toggle() {
        Animated.timing(this.anime.height, {
            toValue: this.anime.expanded ? this._getMinValue() : this._getMaxValue(),
            duration: 300,
        }).start();
        this.anime.expanded = !this.anime.expanded;
    }

    _fetchData(callback) {
        var data = {};
        var data ={
            page: this.state.page,
        }

        AsyncStorage.getItem('userData', (err, result) => {
            if(result) {
                let user_data = [];
               
                user_data = JSON.parse(result);
                
                this.setState({ 
                    access_token: user_data.token
                });
                MiscAPI.getOrderHistory(callback,data,this.state.access_token); 
            }  
        });  
    }

    _fetchMore() {
        if(this.state.count == 10){
            this.setState({ page: this.state.page + 1});
            this.setState({ isLoadingMore: true});
            this._fetchData(responseJson => {
                if(responseJson.status == 200){
                    const data = this.state._data.concat(responseJson.data);
                    this.setState({
                        dataSource: this.state.dataSource.cloneWithRows(data),
                        isLoadingMore: false,
                        _data: data,
                        count: responseJson.count
                    });
                }else{
                    this.setState({ isLoadingMore: false });
                    this.setState({ page: this.state.page - 1});
                }
            });
        }
    }

    componentDidMount() {
       
        this._fetchData(responseJson => {
            
            if(responseJson.status == 200) {

                let ds = new ListView.DataSource({
                    rowHasChanged: (r1, r2) => r1 !== r2,
                });
                
                const data = responseJson.data;
                    
                    this.setState({
                        dataSource: ds.cloneWithRows(data),
                        isLoading: false,
                        _data:data,
                        empty: false,
                        count: responseJson.count,
                    });
                               
            } else {
                this.setState({
                    isLoading: false,
                    empty: true,
                });
            }
        });
    }

    _onRefresh() {
    }

    _onEndReached() {
        this._fetchMore();
    }

    render() {
        
        if (this.state.isLoading) {
            return (
                <Container>
                    <Content>
                        <Spinner color="#0f7482" />
                    </Content>
                </Container>
            );
        }

        if (this.state.empty) {
            return (
                <Container>
                    <Content>
                        <View style={{ alignItems:'center', justifyContent:'center'}}>
                        <Text style={{color: '#0f7482', textAlign:'center',paddingTop: 10, fontSize:16 }}>You’ve not made any order Yet.</Text>
                        </View>
                    </Content>
                </Container>
            );
        }
        
        return (
            <Container>
            <ListView
                legacyImplementation={true}
                //style={styles.listItemBody}
                initialListSize={10}
                onEndReachedThreshold={100}
                pageSize={10}
                enableEmptySections={ true }
                automaticallyAdjustContentInsets={ false }
                dataSource={this.state.dataSource}

                renderRow={(rowData) =>
                    <ListItem style={styles.listItem}>
                        <Card>
                            <CardItem style={{padding: 0, margin: 0}} button>
                                <Left>
                                    {/* <TouchableHighlight underlayColor="transparent" onPress={this.toggle}> */}
                                        <Thumbnail square source={{uri: rowData.order_img }} />
                                    {/* </TouchableHighlight> */}
                                    <Body>
                                        <Text style={styles.title}>{rowData.deal_name}</Text>
                                        <Text style={styles.desc}>{rowData.item_name}</Text>
                                    </Body>
                                </Left>
                            </CardItem>
                            <View style={{ flex:1, flexDirection:'row' }}>
                                <Left style={{margin:10}}>
                                    <Text style={styles.orderTitle}>Price (₦): {rowData.item_cost}</Text>
                                </Left>
                                <Body style={{margin:10}}>
                                    { rowData.order_status == '1' ? 
                                    <View> 
                                        <Text style={{color: 'red',fontSize:22}}>{rowData.order_code}</Text>
                                        <Text style={{color:'red',fontSize:12}}>Redeemed</Text>
                                    </View>
                                    :  <Text style={{color: 'green',fontSize:22}}>{rowData.order_code}</Text> }
                                </Body>
                            </View>
                            {/* <Animated.View style={[styles.body, { height: this.anime.height }]} onLayout={this._initContentHeight} expanded={false}>
                                <Left style={{margin:10}}>
                                    <Text style={styles.orderTitle}>Price (₦): {rowData.item_cost}</Text>
                                </Left>
                                <Body style={{margin:10}}>
                                    { rowData.order_status == '1' ?  <Text style={{color: 'red',fontSize:22}}>{rowData.order_code}</Text> :  <Text style={{color: 'green',fontSize:22}}>{rowData.order_code}</Text> }
                                </Body>
                            </Animated.View> */}
                        </Card>
                    </ListItem>
                }
                onEndReached={ () => this._onEndReached() }
                renderFooter={() => {
                    return (
                    this.state.isLoadingMore &&
                    <View style={{ flex: 1, padding: 10 }}>
                        <Spinner color="#0f7482" />
                    </View>
                    );
                }}
            />
            </Container>
        );
    }
}

const styles = {
    container   : {
        backgroundColor: '#fff',
        margin:0,
        overflow:'hidden'
    },
    title:{
        color: '#0f7482',
        fontSize:16
    },
    orderTitle:{
        color: '#0f7482',
        fontSize:22
    },
    desc:{
        color: '#575757',
        fontSize:14
    },
    listItemBody:{
        backgroundColor: "#f0f0f0", 
        paddingTop: 10,
        padding: 0, 
        margin:0, 
        borderWidth: 0
    },
    listItem: {
        paddingBottom: 0, 
        marginBottom:0, 
        paddingTop: 0, 
        marginTop:0, 
        borderWidth:0,
        borderBottomWidth:0,
    },
    body: {
       flexDirection: 'row'
    }
   
}