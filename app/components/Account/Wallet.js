import React, { Component } from 'react';
import { AsyncStorage, View, StyleSheet, ToastAndroid } from 'react-native';
import { Container, Content, Form, Item, Label, Input, Spinner, Button, Icon, Text, Tab, Tabs, TabHeading } from 'native-base';

import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import Toast from 'react-native-toast-native';

export default class Wallet extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            add_balance: '0',
            balance:'0',
            isLoading: false,
            expiry: '',
            card_number: '',
            cvv: '',
            valid: false,
            card_holder_name: '',
            access_token: null,
        };
        
        this.viewTransaction = this.viewTransaction.bind(this);
    }  

    componentDidMount() {
        // For Tab Navigation From Transaction List
        global.wallet = false;

        AsyncStorage.getItem('userData', (err, result) => {
            if(result) {
                let user_data = [];

                user_data = JSON.parse(result);
                
                this.setState({ 
                    access_token: user_data.token,
                });  
                global.access_token = user_data.token;
                fetch( global.AUTHURL + 'getWallet', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                        'authorization':user_data.token
                    }
                })
                .then(response => response.json())
                .then((response) => {
                    console.log(response);
                    if(response.status == 200){
                        
                        this.setState({
                            balance: response.wallet
                        })
                    }
                })
                .catch(error => {
        
                })
            }
        });
        
        
    }
     
    addCash = () => {

        this.setState({
            isLoading: true
        });

        let data = {
            price:                this.state.add_balance,
            card_holder_name:     this.state.card_holder_name, 
            card_number:          this.state.card_number,
            cvv:                  this.state.cvv,
            expiry:               this.state.expiry
        }

        let errorMsg = '';
        let errorFlag = false;
        
        if(data.card_holder_name == ''){
            errorFlag = true;
            errorMsg = 'Please enter name on card';
        }

        if(data.expiry == ''){
            errorFlag = true;
            errorMsg = 'Please enter expiry date.';
        }

        if(data.cvv == ''){
            errorFlag = true;
            errorMsg = 'Please enter cvv/cvc on card';
        }
        
        if(data.card_number == ''){
            errorFlag = true;
            errorMsg = 'Please enter valid card number';
        }
        
        if(data.price == '0' || data.price == 0) {
            errorFlag = true;
            errorMsg = 'Please enter price to add wallet.';
        }

        if(errorFlag == true){
            this.setState({
                isLoading: false
            });
            
            ToastAndroid.show(errorMsg,Toast.SHORT,Toast.TOP,FAILURE_TOAST_STYLE); 
            
            return true;
        }

        if(data.price > 0) {
            
            fetch( global.AUTHURL + 'addMonyInYourWallet', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    'authorization':this.state.access_token
                },
                body: JSON.stringify( data )
            })
            .then(response => response.json())
            .then((response) => {
                console.log(response);
                if(response.status == 200){
                    
                    this.setState({ balance : parseFloat(this.state.add_balance) + parseFloat( this.state.balance ), add_balance:'0' });

                    this.setState({
                        isLoading: false
                    });
                    
                    let paymeny_notif = {
                        price: data.price,
                        title: "Wallet",
                        post_title: "Your transaction was successful.",
                        description: "₦" + data.price + " has been added to your wallet." 
                    }
                    
                    FCM.presentLocalNotification({
                        // id: "UNIQ_ID_STRING",                           // (optional for instant notification)
                        title: "Your transaction was successful.",                     // as FCM payload
                        body: "₦" + paymeny_notif.price + " " +"- Added to your wallet balance.",                    // as FCM payload (required)
                        sound: "default",                                   // as FCM payload
                        priority: "high",                                   // as FCM payload
                        click_action: "com.myapp.MyCategory",               // as FCM payload - this is used as category identifier on iOS.
                        badge: 0,                                           // as FCM payload IOS only, set 0 to clear badges
                        icon: "ic_launcher",                                // as FCM payload, you can relace this with custom icon you put in mipmap
                        data: paymeny_notif,                                           // extra data you want to throw
                        // show_in_foreground                               // notification when app is in foreground (local & remote)
                    });

                    ToastAndroid.show(response.message,Toast.SHORT); 
            
                } else {
                    this.setState({
                        isLoading: false
                    });

                    ToastAndroid.show(response.message,Toast.SHORT); 
                    
                }
            })
            .catch(error => {
                
            })
        } 
    }

    viewTransaction = () => {
        global.wallet = true;
        this.props.navigation.navigate('Transaction');
    }

    _onChange = (form) => {
        console.log(form);
        if(form.status.expiry == 'valid') {
            this.setState({
                expiry: form.values.expiry,
            });
        }

        if(form.status.cvc == 'valid') {
            this.setState({
                cvv: form.values.cvc,
            });
        }

        if(form.status.number == 'valid') {
            this.setState({
                card_number: form.values.number,
            });
        }

        if(form.status.name == 'valid') {
            this.setState({
                card_holder_name: form.values.name,
            });
        }

    }
   
    render() {

        return (
        <Content style={{flex:1}}>
                    <View style={{ flex:1, alignItems:'center' ,paddingTop:20, paddingBottom:20}}>
                        <Text style={styles.TabHeadingFilterText}> Balance ( ₦ ): { this.state.balance } </Text>
                    </View>
                    <View style={{ margin:15}}>
                        <Form>
                            <Item floatingLabel>
                                <Icon active name='ios-cash-outline' style={styles.icon} /> 
                                <Label> Add Balance </Label>
                                <Input keyboardType='numeric' value={this.state.add_balance} onChangeText={(number) => this.setState({ add_balance: number.replace(/^0+/, '') })} />
                            </Item>
                        </Form>
                    </View>
                    
                    <CreditCardInput
                    requiresName={true}
                    invalidColor={"red"}
                     onChange={this._onChange} />
                    
                    <View style={{ marginLeft:40, marginTop:20, marginRight:35, marginBottom:20}}>
                       <Text style={{color: '#0f7482',fontSize: 18,}}> Quick Cash Add </Text>
                    </View>
                    <View style={{flexDirection: "row", justifyContent:'space-between', marginLeft:40, marginRight:40, marginBottom:10}}>
                        <Button onPress={() => this.setState({ add_balance: '5000' })} style={{ backgroundColor : '#0f7482'}}>
                            <Text style={{ color: "#ffffff",fontSize:18 }}> 5,000 </Text>
                        </Button>

                        <Button onPress={() => this.setState({ add_balance: '10000' })} style={{ backgroundColor : '#0f7482'}}>
                            <Text style={{ color: "#ffffff",fontSize:18 }}> 10,000 </Text>
                        </Button>

                        <Button onPress={() => this.setState({ add_balance: '20000' })} style={{ backgroundColor : '#0f7482'}}>
                            <Text style={{ color: "#ffffff",fontSize:18 }}> 20,000 </Text>
                        </Button>
{/* 
                        <Button onPress={() => this.setState({ add_balance: '500' })} style={{ backgroundColor : '#0f7482'}}>
                            <Text style={{ color: "#ffffff",fontSize:18 }}> 500 </Text>
                        </Button> */}
                    </View>

                    <View style={{ flexDirection: "column", justifyContent:'center', marginLeft:30, marginRight:30}}>
                        <View style={{margin:10}}>
                            {
                                this.state.isLoading == true ?
                                <Spinner color="#0f7482" /> : 
                                <Button block onPress={this.addCash} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                    <Text style={{ color: "#ffffff",fontSize:18 }}> Add Cash </Text>
                                </Button>
                            }
                        </View>
                        <View style={{margin:10}}>
                                <Button block onPress={this.viewTransaction} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                    <Text style={{ color: "#ffffff",fontSize:18 }}> View Transactions </Text>
                                </Button> 
                        </View>
                    </View>
                </Content>
        );
    }
}

const styles = {
    TabHeadingText:{
        color: '#ffffff',
    },
    TabHeadingFilterText:{
        color: '#0f7482',
        justifyContent: 'space-between',
        fontSize: 24,
    },
    TabHeading:{
        backgroundColor: '#0f7482',
    },
    TabHeadingFilter:{
        backgroundColor: '#f5ad2a'
    },
    container: {
        backgroundColor: "#F5F5F5",
        marginTop: 60,
    },
    label: {
        color: "black",
        fontSize: 12,
    },
    input: {
        fontSize: 16,
        color: "black",
    },
    icon:{
        color: '#0f7482',
        paddingTop:5
      }
}