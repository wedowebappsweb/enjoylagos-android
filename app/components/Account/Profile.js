import React, { Component } from 'react';
import { Text, Picker, Modal, View, Image, Dimensions, StyleSheet,NetInfo,Keyboard, TouchableWithoutFeedback, AsyncStorage, TouchableHighlight, Platform, ToastAndroid } from 'react-native';
import { Container, Content, Card, CardItem, Left, TabHeading, Tab, Tabs, Thumbnail, Body, Icon, H2, Form, Item, Label, Input, Button, Spinner, ActionSheet } from 'native-base';
import {CachedImage} from "react-native-img-cache";

import PhoneInput from 'react-native-phone-input';
import PhotoUpload from 'react-native-photo-upload';
import ImageResizer from 'react-native-image-resizer';
import Toast from 'react-native-toast-native';

export default class Profile extends Component {
  
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            email: '',
            password: '',
            confpassword: '',
            phone: '',
            old_password: '',
            isLoading: false,
            modalVisible: false,
            avatar: 'http://enjoylagos.net/wp-content/uploads/default_avatar.jpg',
            updateProfile: false,
            access_token: null,
            passwordSuccessModal: false,
            passwordInputType: true
        };

    }   
    
    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    componentDidMount() {

        AsyncStorage.getItem('userData', (err, result) => {
            if(result) {
                let user_data = [];
                user_data = JSON.parse(result);

                this.setState({ 
                    name: user_data.name,
                    email: user_data.email,
                    phone: user_data.phone,
                    access_token: user_data.token
                });
                
                if(user_data.avatar == '' || user_data.avatar == null) {
                    this.setState({
                        avatar: 'http://enjoylagos.net/wp-content/uploads/default_avatar.jpg',
                    })
                } else {
                    this.setState({
                        avatar:user_data.avatar
                    })
                }
            }
        });
    }

    updateProfile = () => {
        this.setState({
            isLoading: true
        });
        const data = {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            avatar: this.state.avatar,
            token: this.state.access_token
        }
        const json = JSON.stringify(data);

        fetch( global.AUTHURL + 'updateUserData', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization':this.state.access_token
                },
                body: json
            })
            .then(response => response.json())
            .then((response) => {
                if(response.status == 200){
                    AsyncStorage.setItem('userData',  JSON.stringify(data));
                    ToastAndroid.show(response.message,Toast.SHORT); 
                    this.setState({
                        isLoading: false
                    });
                 
                } else {
                    this.setState({
                        isLoading: false
                    });
                    ToastAndroid.show(response.message,Toast.SHORT); 
                }
              })
            .catch(error => {
                console.log(error);
            })
        
    }

    changePassword = () => {
        
        const data = {
            password: this.state.password,
            old_password: this.state.old_password,
        }

        let errorMsg = '';
        let errorFlag = false;
        
        if(this.state.old_password == '') {
            errorFlag = true;
            errorMsg = 'Please enter old Password';
        }

        if(data.password != this.state.confpassword){
            errorFlag = true;
            errorMsg = 'Password does not match.';
        }

        if(data.password.length < 8){
            errorFlag = true;
            errorMsg = 'Password must be at least 8 characters long';
        }

        if(data.password == ''){
            errorFlag = true;
            errorMsg = 'Please enter Password';
        }

        if(errorFlag) {
            
            this.setState({
                isLoading: false
            });
            
            ToastAndroid.show( errorMsg,Toast.SHORT); 
            
            return true;
        }

        
        const json = JSON.stringify(data);

        AsyncStorage.getItem('user_token', (err, result) => {
            
            fetch( global.AUTHURL + 'updateUserPass', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    'authorization':result
                },
                body: json
            })
            .then(response => response.json())
            .then((response) => {
                
                if(response.status == 200){
                 
                    this.setState({
                        isLoading: false
                    });
                    
                    this.setModalVisible(false);
                    this.setState({passwordSuccessModal:true});
                 
                } else {
                    this.setState({
                        isLoading: false
                    });
                    
                    ToastAndroid.show( response.message,Toast.SHORT); 
            
                }
              })
            .catch(error => {
            
            })
        });
    }

    _updateProfilePic = (response) => {
        if(response.didCancel != true) {
            ImageResizer.createResizedImage( response.uri, 400, 400, 'JPEG', 100).then((response) => {
                
                this.setState({
                    updateProfile: true
                });
                
                const now = new Date();
                let imgName = now.getTime()+'.jpg';

                var photo = {
                    uri: response.uri,
                    type: 'image/jpeg',
                    name: imgName,
                };
                
                console.log('PHOTO - COMPRESS & UPLOAD');
                console.log(photo);

                const form = new FormData();
                form.append('user_img', photo);
                
                fetch( global.AUTHURL + 'updateUserPic', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Accept': 'application/json',
                        'authorization':this.state.access_token
                    },
                    body: form
                })
                .then(response => response.json())
                .then((response) => {
                    this.setState({
                        updateProfile: false
                    })
                    if(response.status == 200){
                        this.setState({avatar: response.thumb});
                        const data = {
                            name: this.state.name,
                            email: this.state.email,
                            phone: this.state.phone,
                            avatar: response.thumb,
                            token: this.state.access_token
                        }
                        AsyncStorage.setItem('userData',  JSON.stringify(data));
                        ToastAndroid.show( response.message,Toast.SHORT); 
                    } else {
                        this.setState({
                            isLoading: false
                        });
                        ToastAndroid.show( response.message,Toast.SHORT);                        
                    }
                })
                .catch(error => {
                    ToastAndroid.show( error.message,Toast.SHORT); 
                })
            }).catch((err) => {
                // Oops, something went wrong. Check that the filename is correct and
                // inspect err to get more details.
            });    
        }
    }

        showhidePassword = () => {
            if(this.state.passwordInputType) {
              this.setState({passwordInputType: false});
            } else {
              this.setState({passwordInputType: true});
            }
          }
        
        render() {
            console.log(this.state.avatar);
            return (
                <Container>
                <Content>
                    <View style={{ alignItems:'center' ,paddingTop:20,paddingBottom:20}}>
                        <PhotoUpload
                            width= {300}
                            quality= {40}
                            onPhotoSelect={avatar => {
                                if (avatar) {
                                {/* console.log('Image base64 string: ', avatar) */}
                                }
                            }}
                            onResponse={(response) => this._updateProfilePic(response)}
                            //onResponse={(response) => console.log(response)}
                            onStart={() => this.setState({updateProfile: true})}
                            onCancel={() => this.setState({updateProfile: false})}
                            >
                            
                            { this.state.updateProfile ? <Spinner color="#0f7482" /> :
                            <View>
                            <Image
                                style={{
                                paddingVertical: 30,
                                width: 150,
                                height: 150,
                                borderRadius: 75
                                }} 
                                resizeMode='cover'
                                source={{
                                uri: this.state.avatar
                                }}
                            />
                            </View>
                            }
                        </PhotoUpload>
                    </View>
                    <View>
                        <Form>
                            <Item floatingLabel>
                                <Icon active name='ios-person' style={styles.icon} /> 
                                <Label style={{paddingTop: 6}}>Name</Label>
                                <Input  value={this.state.name} onChangeText={(text) => this.setState({ name: text })} />
                            </Item>
                            <Item floatingLabel>
                                <Icon active name='ios-mail' style={styles.icon} /> 
                                <Label style={{paddingTop: 6}}>Email</Label>
                                <Input editable={false} value={this.state.email} onChangeText={(text) => this.setState({ email: text })} />
                            </Item>
                        
                            <Item>
                                <View style={{flexDirection:"row", paddingTop: 7, marginTop:7, paddingBottom:10,  }}>
                                    <View style={{flex:0.8, flexDirection: "row"}}>
                                        <Icon active name='ios-call' style={styles.icon} /> 
                                        <Label>Phone</Label>
                                    </View>
                                    <View style={{flex:2.5}}>
                                        <PhoneInput
                                            ref={(ref) => { this.phone = ref; }}
                                            onPressFlag={this.onPressFlag}
                                            style={{marginLeft:8}}
                                            onChangePhoneNumber= {(number) => this.setState({phone: number})}
                                            offset={20}
                                            value={this.state.phone}
                                            initialCountry='ng'
                                        />
                                    </View>
                                </View>
                            </Item>
                        </Form>
                    </View>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setModalVisible(false)
                        }}>
                        <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
                        <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                            <View style={{ padding: 30, backgroundColor: '#fff', height: undefined, width: undefined }}>
                                <Form>
                                    <H2>Change Password!</H2>
                                    <View style={{flexDirection:'row',borderBottomColor:'gray'}}>
                                        <Item floatingLabel style={{flex:1,borderBottomWidth:.4, borderBottomColor:'gray'}}>
                                        <Icon name='ios-lock' style={styles.icon} />
                                        <Label style={{paddingTop:3}}>Old Password</Label>
                                        <Input secureTextEntry={this.state.passwordInputType} onChangeText={(text) => this.setState({ old_password: text })} />
                                        </Item>
                                        <View style={{paddingTop:40, flex:.1, borderBottomColor:'gray', borderBottomWidth:.5}}>
                                            <Icon name={this.state.passwordInputType ? 'ios-eye' : 'ios-eye-off'} style={{ color:'#0f7482'}} onPress={this.showhidePassword} />
                                        </View>
                                    </View>
                                    <Item floatingLabel>
                                        <Icon active name='ios-lock' style={styles.icon} /> 
                                        <Label>New Password</Label>
                                        <Input type="password" secureTextEntry={this.state.passwordInputType} onChangeText={(text) => this.setState({ password: text })} />
                                    </Item>
                                    <Item floatingLabel>
                                        <Icon active name='ios-lock' style={styles.icon} /> 
                                        <Label>Confirm Password</Label>
                                        <Input type="password" secureTextEntry={this.state.passwordInputType} onChangeText={(text) => this.setState({ confpassword: text })} />
                                    </Item>

                                    <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                                    
                                        <View style={{margin:10}}>
                                            {
                                                this.state.isLoading == true ?
                                                <Spinner color="#0f7482" /> : 
                                                <Button block success onPress={this.changePassword} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                                    <Text style={{ color: "#ffffff", fontSize:18 }}> Update </Text>
                                                </Button>
                                            }
                                        </View>
                                        <View style={{margin:10}}>
                                            <Button block danger onPress={() => { this.setModalVisible(false) }} style={{ padding:10 }}>
                                                <Text style={{ color: "#ffffff",fontSize:18 }}> Cancel </Text>
                                            </Button>
                                        </View>
                                    </View>
                                </Form>
                            </View>
                        </View>
                        </TouchableWithoutFeedback>
                    </Modal>

                    <Modal
                    animationType="slide"
                    transparent={true}
                    style={{marginTop: 22}}
                    visible={this.state.passwordSuccessModal}
                    onRequestClose={() => {
                        // alert('Modal has been closed.');
                        this.setState({ passwordSuccessModal: false})
                    }}>
                        <View style={{ flex: 1, justifyContent: 'center', padding: 20, backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                            <View style={{ padding: 30, backgroundColor: '#fff' }}>
                                <Form>
                                    {/* <Text style={{color: 'red', fontSize: 16, textAlign: 'center'}}>Insufficient funds in your wallet.</Text> */}
                                    <Text style={{fontSize: 18, textAlign: 'center'}}> Password Updated Successfully.</Text>

                                    <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                                    
                                        <View style={{margin:10}}>
                                            <Button block success onPress={() => {
                                                this.setState({ passwordSuccessModal: false });
                                            }} style={{ backgroundColor : '#0f7482', padding:10 }}>
                                                <Text style={{ color: "#ffffff",fontSize:14 }}> Okay </Text>
                                            </Button>
                                        </View>
                                    </View>

                                </Form>
                            </View>
                        </View>
                    </Modal>  
                    
                    <View style={{ flexDirection: "row", paddingTop:10, justifyContent:'center'}}>
                        
                        <View style={{margin:5}}>
                            <Button block onPress={() => { this.setModalVisible(true) }} style={{ backgroundColor : '#0f7482', width:170 }}>
                                <Text style={{ color: "#ffffff",fontSize:18 }}> Change Password </Text>
                            </Button>
                        </View>

                        <View style={{margin:5}}>
                            {
                                this.state.isLoading ?
                                <View style={{marginTop:-15, width:170}}><Spinner color="#0f7482" /></View> : 
                                <Button block onPress={this.updateProfile} style={{ backgroundColor : '#0f7482', width:170 }}>
                                    <Text style={{ color: "#ffffff",fontSize:18 }}> Update </Text>
                                </Button>
                            }
                        </View>
                        
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = {
    TabHeadingText:{
        color: '#ffffff',
    },
    TabHeadingFilterText:{
        color: '#ffffff',
    },
    TabHeading:{
        backgroundColor: '#0f7482',
    },
    TabHeadingFilter:{
        backgroundColor: '#f5ad2a'
    },
    promotionText: {
        fontSize: 16,
        color: '#575757',
        padding: 20,
        textAlign:'center'
      },
      icon:{
        color: '#0f7482'
      }
}