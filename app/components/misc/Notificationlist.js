import React, { Component } from 'react'
import { Text, View, Image, Dimensions, BackHandler, ListView, StyleSheet, NetInfo, RefreshControl, AsyncStorage } from 'react-native';
import { Container, Content, List, ListItem, Left, Header, Item, Icon, Input, Button, Thumbnail, Body, Card, CardItem, Right, Spinner } from 'native-base';

import {
    Analytics,
    Hits as GAHits,
    Experiment as GAExperiment
  } from 'react-native-google-analytics';
  import DeviceInfo from 'react-native-device-info';
  var ga = this.ga = null;
  
class Notificationlist extends Component {

  constructor(props) {
    super(props);
    this.state = {
        isLoading: true,
        dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
     
    };
    this.navigateDealDetails = this.navigateDealDetails.bind(this);
  }

    navigateDealDetails(rowData) {
        this.props.navigation.navigate('Dealdetails',{rowData: rowData});
    }

    componentDidMount() {
        
        let clientId = DeviceInfo.getUniqueID();
        
        ga = new Analytics(GOOGLE_ANALYTICS, clientId, 1, DeviceInfo.getUserAgent());
        
        var screenView = new GAHits.ScreenView(
          'EnjoyLagos App',
          'Notification Screen',
          DeviceInfo.getReadableVersion(),
          DeviceInfo.getBundleId()
        );
        ga.send(screenView);

        
        AsyncStorage.getItem('notifDeal', (err, result) => {
            if(result) {
                if ( result.length > 2 ) {
                    let data = JSON.parse(result);
                    
                    this.setState({
                        dataSource: this.state.dataSource.cloneWithRows(data),
                        isLoading: false
                    });
                } else {
                    this.setState({
                        dataSource: null,
                        isLoading: false
                    });   
                }
            } else {
                this.setState({
                    dataSource: null,
                    isLoading: false
                });
            }
        });        
    }

    deleteRow(secId, rowId, rowMap) {
        
        rowMap[`${secId}${rowId}`].props.closeRow();
        const newData = [...this.state.dataSource];
        newData.splice(rowId, 1);
        
        if(newData) {
            this.setState({ dataSource: newData });
        } else {
            this.setState({ dataSource: null });
        }
       
        AsyncStorage.setItem('notifDeal', JSON.stringify(newData));    
    }

  render () {
    if(this.state.isLoading) {
        return (
            <Content>
                <Spinner color="#0f7482" />
            </Content>
        );
    }

    if(this.state.dataSource == '' || this.state.dataSource == null  ) {
        return (
            <Content>
                <View style={{ alignItems: 'center', padding: 20}}>
                    <Text style={styles.dealTitle}> No notifications here at the moment. </Text>
                </View>
            </Content>
        );
    }

    if(this.state.dataSource != null) {
        return (
        <Content style={{ paddingTop: 0, paddingBottom:0}}>
        <List
                legacyImplementation={true}
                style={styles.listItemBody}
                // onEndReachedThreshold={100}
                enableEmptySections={ true }
                // automaticallyAdjustContentInsets={ false }
                dataSource={this.state.dataSource}

                renderRow={(rowData) =>
                    <ListItem style={styles.listItem}>
                        <Card>
                            { rowData.thumbnail != null ?
                                <CardItem style={{padding: 0, margin: 0}} button onPress={() => this.navigateDealDetails(rowData)}>
                                
                                    {rowData.redeemed == "Y" ? 
                                    
                                    <Left>
                                        <Thumbnail square source={{uri: rowData.thumbnail }} />
                                        <Body>
                                            <Text style={{color: '#0f7482',fontSize:16}}>{rowData.noti_title}</Text>
                                            <Text style={{color: '#575757',fontSize:12}}>{rowData.noti_body}</Text>
                                        </Body>
                                    </Left>
                                    :
                                    <Left>
                                    <Thumbnail square source={{uri: rowData.thumbnail }} />
                                        <Body>
                                            <Text style={styles.dealTitle}>{rowData.post_title}</Text>
                                            <Text style={styles.dealDesc}>{rowData.description}</Text>
                                        </Body>
                                    </Left>
                                    }
                                
                            </CardItem>    
                             : 
                            <CardItem style={{padding: 0, margin: 0}} button>
                                <Left>
                                    <Text style={{fontSize:18,  color: '#0f7482',}}>  ₦{rowData.price} </Text>
                                    <Body>
                                        <Text style={styles.dealTitle}>{rowData.post_title}</Text>
                                        <Text style={styles.dealDesc}>{rowData.description}</Text>
                                    </Body>
                                </Left>
                            </CardItem>
                            }
                        </Card>
                    </ListItem>
                }
                onEndReached={ () => this._onEndReached() }
                renderFooter={() => {
                    return (
                    this.state.isLoadingMore &&
                    <View style={{ flex: 1, padding: 10 }}>
                        <Spinner color="#0f7482" />
                    </View>
                    );
                }}

                renderLeftHiddenRow={data =>
                    console.log(data)
                }
                  renderRightHiddenRow={(data, secId, rowId, rowMap) =>
                    <Button style={{ backgroundColor: 'red', marginTop: 10, marginBottom: 10, marginRight: 10}} full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                      <Icon active name="trash" />
                    </Button>}
                  leftOpenValue={0}
                  rightOpenValue={-75}
            />
        </Content>
        )
    }    
  }
}


const styles = {
    listItemBody:{
        backgroundColor: "#f0f0f0", 
        paddingTop: 10,
        paddingLeft: 10,
        padding: 0, 
        margin:0, 
        borderWidth: 0
    },
    listItem: {
        backgroundColor: "#f0f0f0", 
        paddingBottom: 0, 
        marginBottom:0, 
        paddingTop: 0, 
        marginTop:0, 
        borderWidth:0,
        borderBottomWidth:0,
    },
    dealTitle:{
        color: '#0f7482',
        fontSize:16
    },
    dealDesc:{
        color: '#575757',
        fontSize:14
    },
    TabHeadingText:{
        color: '#ffffff',
    },
    TabHeadingFilterText:{
        color: '#ffffff',
    },
    TabHeading:{
        backgroundColor: '#0f7482',
    },
    TabHeadingFilter:{
        backgroundColor: '#f5ad2a'
    }
}

export default Notificationlist;