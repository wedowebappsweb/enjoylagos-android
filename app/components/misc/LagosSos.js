import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet, Modal } from 'react-native'
import { Container, Content, Card, CardItem, Left, Thumbnail, Body, Icon} from 'native-base';

class LagosSos extends Component {

  constructor(props) {
    super(props);
    this.openthePopup = this.openthePopup.bind(this);
    this.navigateATMList = this.navigateATMList.bind(this);
    this.navigateEmergencyNumberList = this.navigateEmergencyNumberList.bind(this);
    this.navigatePoliceStationList = this.navigatePoliceStationList.bind(this);
    this.state = {
        showModel : false
    }
  }

  openthePopup(){
    this.setState({
        showModel: true
    }); 
  }

    navigateATMList() {
        this.props.navigation.navigate('ATMList');
    }
    navigateEmergencyNumberList() {
        this.props.navigation.navigate('EmergencyNumberList');
    }
    navigatePoliceStationList() {
        this.props.navigation.navigate('PoliceStationList');
    }

  render () {
    const { navigate } = this.props.navigation; 
    return (
        <View style={{padding: 17}}>
            <Card>
                <CardItem button onPress={this.navigateATMList} >
                    <Body>
                        <Text style={styles.sosText}>UBA ATM/Branch Locations</Text>
                    </Body>
                </CardItem>
            </Card>
            <Card>
                <CardItem button onPress={this.navigatePoliceStationList }>
                    <Body>
                        <Text style={styles.sosText}>Nearest Police Station</Text>
                    </Body>
                </CardItem>
            </Card>
            <Card>
                <CardItem button onPress={this.navigateEmergencyNumberList }>
                    <Body>
                        <Text style={styles.sosText}>Emergency Numbers</Text>
                    </Body>
                </CardItem>
            </Card>

            <Modal
                transparent={true}
                style={styles.model}
                onRequestClose= {() => {
                    this.setState({
                        showModel: false
                    });
                }}
                visible={this.state.showModel}>
                <View style={{backgroundColor:'rgba(0,0,0,0.5)', flex:1}}></View>
                <View style={styles.modelin}>
                    <Text style={styles.modelText}>This feature will be available in the next version. Thank you!</Text>
                </View>
            </Modal>

        </View>
    )
  }
}

const styles = {
  sosText:{
    color: '#575757',
    fontSize: 20,
  }, 
  model:{
    backgroundColor:'rgba(0,0,0,0.5)',
    flex: 1,
    marginTop: 100,
    marginLeft:20,
    marginRight:20,
    height: 200,
    flexDirection:'row'
  },
  modelin: {
    position: 'absolute', 
    left: 0, 
    right: 0, 
    bottom: 0,
    top: 0,
    height: 120,
    backgroundColor:"#ffffff",
    justifyContent:"center",
    marginTop: 200,
    marginLeft:20,
    marginRight:20,
    padding: 20,
  }, 
  modelText:{
    fontSize: 16,
    fontWeight: 'bold'
  }
}

export default LagosSos;