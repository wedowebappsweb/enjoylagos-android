import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet } from 'react-native'
import { Container, Content, Card, CardItem, Left, Thumbnail, Body, Icon, Title} from 'native-base';

const SunIcon = require('../img/Weather-Sun-icon.png')

import MiscAPI from '../../utils/Misc';

class LagosInfoHeader extends Component {

  constructor(props) {
    super(props);

    this.state = {
        usd:0,
        gbp:0,
        eur:0,
        temp:0.00,
        day: '',
        date: '',
        weather: '',
        weatherIcon: ''
    }

  }

  _fetchData(callback) {
    MiscAPI.getCurrencyRates(callback); 
  }

  componentWillMount(){
    this._fetchData(responseJson => {
        if(responseJson.status == 200){
            var data = responseJson.data;
            this.setState({ 
                isLoading: false,
                usd: data.usd,
                eur: data.eur,
                gbp: data.gbp,
                temp: data.temp,
                day: data.day,
                date:data.date,
                weather: data.weather,
                weatherIcon: data.weather_icon
            });
        }        
    });
  }

  render () {
    return (

        <View style={{flexDirection: 'row', height: 140, padding: 10, backgroundColor: '#0f7482'}}>
            <View style={{flex: 3, padding: 10, justifyContent: 'center'}} >
                <View style={styles.dtContainer}>
                    <Text style={styles.title}>{this.state.day}</Text>
                    <Text style={styles.Text}>{this.state.date}</Text>
                </View>
                <View style={styles.currencyContainer}>
                    <View style={{flex: 1}}>
                        <Text style={styles.title}>USD</Text>
                        <Text style={styles.Text}>{this.state.usd}</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.title}>GBP</Text>
                        <Text style={styles.Text}>{this.state.gbp}</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.title}>EUR</Text>
                        <Text style={styles.Text}>{this.state.eur}</Text>
                    </View>
                </View>

            </View>
            <View style={styles.weatherContainer} >
                { this.state.weatherIcon ? <Image source={{uri: this.state.weatherIcon}} style={{marginBottom: 10, width: 60, height: 60}} /> : null }
                <Text style={styles.Text}>{this.state.temp}°C</Text>
                <Text style={styles.Text}>{this.state.weather}</Text>
            </View>
        </View>

    )
  }
}

const styles = {
    Text:{
        color: '#fff',
        fontSize: 16
    },
    title: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16
    },
    currencyContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    dtContainer: {
        paddingBottom: 20
    }, 
    weatherContainer: {
        flex: 1, 
        justifyContent: 'flex-end',
        marginBottom: 8,
        alignItems: 'center'
    }
}

export default LagosInfoHeader;