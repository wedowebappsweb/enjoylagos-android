import React, { Component } from 'react'
import { AsyncStorage } from 'react-native'

export default class DealsAPI extends Component{

    static addToFav(id, deal_id,callback, access_token){
        var data = { 'item_id' : id, 'deal_id' : deal_id };
        const json = JSON.stringify(data);

        fetch(AUTHURL+'addTofav', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'authorization':access_token 
            },
            body: json
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        
        });
    }
 
    static getDealsList(callback,data,access_token){
        // let returnRes = '';
        let jsondata = JSON.stringify(data);

        fetch(APIURL+'getDeals', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'authorization':access_token 
            },
            body: jsondata
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        
        });
    }

    static getDealItems(callback, post_id){

        var data = { 'post_id' : post_id };
        const json = JSON.stringify(data);

        AsyncStorage.getItem('user_token', (err, result) => {
            
            fetch(AUTHURL+'getDealItems', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    'authorization':result
                },
                body: json
            })
            .then(response => response.json())
            .then(callback)
            .catch(error => {
            
            });
        });
    }

    static getDealVendor(callback, deal_id, id){
        
        var data = { 'deal_id' : deal_id, 'item_id' : id };
        const json = JSON.stringify(data);

        AsyncStorage.getItem('user_token', (err, result) => {
            
            fetch(AUTHURL+'getDealDetails', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    'authorization':result
                },
                body: json
            })
            .then(response => response.json())
            .then(callback)
            .catch(error => {
            
            });
        });
    }

    static getDealCategories(callback, post_id){
        
        var data = { 'post_id' : post_id };
        const json = JSON.stringify(data);

        fetch(APIURL+'getDealCategories', { 
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'os':'i',
                'appv':'1.0',
                'osv':'7.0',
                'lang':'EN',
                'devicetoken':'1234567890',
                'devicename':'OnePlus 3T',
            },
            body: json
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        
        });
    }

}