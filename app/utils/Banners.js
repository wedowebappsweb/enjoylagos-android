import React, { Component } from 'react'

export default class BannersAPI extends Component{

    static getDealsList(callback){

        let returnRes = '';

        fetch(APIURL+'get_banners', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              'os':'i',
              'appv':'1.0',
              'osv':'7.0',
              'lang':'EN',
              'devicetoken':'1234567890',
              'devicename':'OnePlus 3T',
            },
            body:''
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        console.error(error);
        });
    }

}