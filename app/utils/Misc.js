import React, { Component } from 'react'

export default class MiscAPI extends Component{

    static getCurrencyRates(callback){

        let returnRes = '';

        fetch(APIURL+'getCurrencyRates', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              'os':'i',
              'appv':'1.0',
              'osv':'7.0',
              'lang':'EN',
              'devicetoken':'1234567890',
              'devicename':'OnePlus 3T',
            },
            body:''
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        
        });
    }

    static getFrame(callback,screen){
        var data = {
            screen: screen
        }
        json = JSON.stringify(data);
        fetch(APIURL+'getFrameUrl', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body:json
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        
        });
    }

    static getPageContent(callback,page){
        var data = {
            page: page
        }
        json = JSON.stringify(data);
        fetch(APIURL+'staticPage', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body:json
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        
        });
    }

    static getINFOList(callback,data,apiName){
        
        let jsondata = JSON.stringify(data);

        fetch(APIURL+ apiName, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'os':'i',
              'appv':'1.0',
              'osv':'7.0',
              'lang':'EN',
              'devicetoken':'1234567890',
              'devicename':'OnePlus 3T',
            },
            body:jsondata
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        
        });
    }

    static getTransactionHistory(callback,data,token) {
        
        let jsondata = JSON.stringify(data);
        
        fetch( global.BASEURL +'/wp-json/auth/getWalletHistory', { 
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'authorization':token
            },
            body: jsondata
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
            
        });
    }

    static getOrderHistory(callback,data,token) {
        
        let jsondata = JSON.stringify(data);
        
        fetch( global.BASEURL +'/wp-json/auth/getAllOrder', { 
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'authorization':token
            },
            body: jsondata
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
            
        });
    }

}
