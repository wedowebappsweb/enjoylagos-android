import React, { Component } from 'react'

export default class NewsAPI extends Component{

    static getNewsList(callback,data){

        let returnRes = '';
        let json = JSON.stringify(data);

        fetch(APIURL+'getNews', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              'os':'i',
              'appv':'1.0',
              'osv':'7.0',
              'lang':'EN',
              'devicetoken':'1234567890',
              'devicename':'OnePlus 3T',
            },
            body:json
        })
        .then(response => response.json())
        .then(callback)
        .catch(error => {
        
        });
    }

}