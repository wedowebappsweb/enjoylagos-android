import React, { Component } from 'react';
import { AsyncStorage, AppState, Alert, PermissionsAndroid, NetInfo, Platform, ToastAndroid } from 'react-native';
import { Toast } from 'native-base';
import { Root } from "native-base";
import { AppRoot, Tabs } from './app/config/router';
import { AppHeader } from './app/components/Header';
import TimerMixin from 'react-timer-mixin';
import PushNotification from 'react-native-push-notification';
import SplashScreen from 'react-native-splash-screen';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';

FCM.on(FCMEvent.Notification, async (notif) => {
    // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
    if(notif.local_notification){
      //this is a local notification
    }
    
    if(notif.opened_from_tray){
      //iOS: app is open/resumed because user clicked banner
      //Android: app is open/resumed because user clicked banner or tapped app icon
    }
    // await someAsyncCall();

    if(Platform.OS ==='ios'){
      //optional
      //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623013-application.
      //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
      //notif._notificationType is available for iOS platfrom
      switch(notif._notificationType){
        case NotificationType.Remote:
          notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
          break;
        case NotificationType.NotificationResponse:
          notif.finish();
          break;
        case NotificationType.WillPresent:
          notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
          break;
      }
    }
});
FCM.on(FCMEvent.RefreshToken, (token) => {
    
});

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null,
    };
    
  }

  componentDidMount() {

    SplashScreen.hide();
    FCM.requestPermissions().then(()=>{
      // Hello World 
    }).catch(()=>
      console.log('notification permission rejected')
    );

    // const granted = PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
    // if (granted) {
    //   console.log( "You can use the ACCESS_FINE_LOCATION" )
    // } else {
    //   console.log( "ACCESS_FINE_LOCATION permission denied" )
    // }
    
    navigator.geolocation.getCurrentPosition((position) => {
      this.setState({
        latitude : position.coords.latitude,
        longitude : position.coords.longitude,
      });
    }, (err) => {
        console.log(err.message);
    }, { enableHighAccuracy: true, timeout: 10000});

    setTimeout(() => {
      FCM.getFCMToken().then(token => {
          global.deviceToken = token;
          var DeviceInfo = require('react-native-device-info');

          var data = {
            lat : this.state.latitude,
            lng : this.state.longitude,
            token : token,
            'os': Platform.OS,
            'appv': DeviceInfo.getVersion(),
            'osv': DeviceInfo.getSystemVersion(),
            'lang':DeviceInfo.getDeviceLocale(),
            'devicetoken' : token,
            'devicename': DeviceInfo.getBrand()+' '+DeviceInfo.getModel(),
          };

          const json = JSON.stringify(data);
          fetch('http://enjoylagos.net/wp-json/api/registerDevice', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
            },
            body: json
        })
        .then((response) => response.json())
        .then((response) => {

        });
      });
    }, 1000);
    
    NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {

        } else {
          ToastAndroid.show("Please check your internet connection.",Toast.SHORT); 
        }
        // console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      });
  }

  render() {
    return (
      <AppRoot />
    );
  }

}

export default () =>
<Root>
    <App />
</Root>; 